//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GooglePlaces
import Firebase


class UpdateSparkViewController : UIViewController, UITextFieldDelegate , UITextViewDelegate ,CatogoriesTableViewControllerDelegate, PlaceSearchResultsControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate , UINavigationControllerDelegate{
    
    
    @IBOutlet weak var sparktitle: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var coverimage: CustomizableImageView!
    
    @IBOutlet weak var Location: SkyFloatingLabelTextField!
    
    @IBOutlet weak var catogories: SkyFloatingLabelTextField!
    
    @IBOutlet weak var descriptiontextview: UITextView!
    
    var spark : Spark?
    var coverimageupdate: Bool = false
    var tags: [String] = []
 
    var PlaceSearchResultsControllervar:PlaceSearchResultsController!
    var CatogoriesTableViewControllervar:CatogoriesTableViewController!
    
    var netService = NetworkingService()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        
        setTapGestureRecognizerOnView()
        sparktitle.delegate = self
        Location.delegate = self
        catogories.delegate = self
        descriptiontextview.delegate = self
        descriptiontextview!.layer.borderWidth = 1
        descriptiontextview!.layer.borderColor = UIColor.lightGray.cgColor
        
        
        PlaceSearchResultsControllervar = PlaceSearchResultsController()
        PlaceSearchResultsControllervar.delegate = self
        CatogoriesTableViewControllervar = CatogoriesTableViewController()
        CatogoriesTableViewControllervar.delegate = self
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapcover(_:)))
        coverimage.addGestureRecognizer(tap)
        coverimage.isUserInteractionEnabled=true
        
        
        
        renderspark()
    }
    
    private func renderspark()
    {
        
        self.coverimage.sd_setImage(with: URL(string: (self.spark?.cover!)!), placeholderImage: UIImage(named: "default"))
        print("here")
        self.sparktitle.text = self.spark?.title
        self.Location.text = self.spark?.location
        self.catogories.text = self.spark?.catogories
        
        self.descriptiontextview.text = self.spark?.description
        self.tags = self.descriptiontextview.resolveHashTags()
    }
    
    func tapcover(_ sender:UITapGestureRecognizer) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = coverimage
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String,location_details : [ String : String ]) {
        Location.text = title
        self.spark?.location = title
        
        if let country = location_details["country"] {
            self.spark?.country = country
        }
        if let state = location_details["state"] {
            self.spark?.state = state
        }
        if let city = location_details["city"] {
            self.spark?.city = city
        }
        if let postal_code = location_details["postal_code"] {
            self.spark?.postal_code = postal_code
        }
        if let route = location_details["route"] {
            self.spark?.route = route
        }
        
        
        self.spark?.lon = lon
        self.spark?.lat = lat
        
        print(title)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    
    func imageLayerForGradientBackground(myview : UIView) -> UIImage {
        
        var updatedFrame = myview.bounds
        // take into account the status bar
        updatedFrame.size.height += 20
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func selectedcatogoriesprotocol(_ catogoriesarray: [String]) {
        print(catogories)
        catogories.text = catogoriesarray.joined(separator: ",")
    }
    
    
    @IBAction func updateSpark(_ sender: Any) {
       
        let sparkloc = Location?.text!.lowercased()
        let sparktile = sparktitle?.text!
        let sparkcat = catogories?.text!
        let sparkdes = descriptiontextview?.text!
        var data : Data?
        spark?.title = sparktile
        spark?.catogories = sparkcat
        spark?.description = sparkdes
        spark?.tags = self.tags
        
        if(coverimageupdate)
        {
            data = UIImageJPEGRepresentation(coverimage.image!, 0.2)!
        }
        
        if (sparkloc?.isEmpty)! || (sparktile?.isEmpty)! || (sparkcat?.isEmpty)! || (sparkdes?.isEmpty)!  {
            print("empty fields")
        }else {
            netService.updateSpark(spark: spark, pictureData: data, completed: {
               // let storyBoard = UIStoryboard(name: "Main", bundle: nil)
              //  let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
             //   SparkdetailsViewController.thisspark = self.spark
             //   self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
            })
         }
        
        self.view.endEditing(true)
        
        
    }
    
    
    
}

extension UpdateSparkViewController {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.coverimage.image = chosenImage
            coverimageupdate = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        animateView(up: true, moveValue: 150)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateView(up: false, moveValue:
            150)
    }
    func textViewDidChange(_ textView: UITextView) {
        self.tags = textView.resolveHashTags()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sparktitle?.resignFirstResponder()
        Location?.resignFirstResponder()
        catogories?.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField != catogories && textField != Location)
        {
            
            animateView(up: true, moveValue: 80)
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == Location)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(PlaceSearchResultsControllervar, animated: false)
            return false
        }
        else if(textField == catogories)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(CatogoriesTableViewControllervar, animated: false)
            return false
            
        }
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField != catogories && textField != Location)
        {
            animateView(up: false, moveValue:
                80)
        }
    }
    
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    
}

