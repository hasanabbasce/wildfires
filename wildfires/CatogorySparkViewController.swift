//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SCLAlertView
import Firebase
import FBSDKShareKit

class CatogorySparkViewController : UIViewController ,UITableViewDelegate ,UITableViewDataSource , SparkTableViewCellDelegate   {
    
    
    
   
    @IBOutlet weak var tableView: UITableView!
    let postDate = NSDate().timeIntervalSince1970 as NSNumber
    var SparkArray: [Spark] = []
    var netService = NetworkingService()
    var category : String = ""
     var tableempty : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = category
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        let backItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(back))
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
       
        self.navigationItem.leftBarButtonItem = backItem
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 250
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
      fetchAllSparksbycategory()
    }
    
     func back() {
        dismiss(animated: true, completion: nil)
    }
    
    
    private func fetchAllSparksbycategory(){
        netService.fetchAllSparksbycategory(category: (self.category)) {(sparks) in
            self.SparkArray = sparks
            self.SparkArray.sort(by: { (spark1, spark2) -> Bool in
                Int(spark1.postDate!) > Int(spark2.postDate!)
            })
            self.SparkArray = self.netService.isblockedspark(sparks: self.SparkArray)
            self.tableView.reloadData()
            // self.tableView.frame.height = self.tableView=
        }
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int = 0
        if(self.SparkArray.count == 0)
        {
            tableempty = true
            count = 1
        }
        else
        {
            count = self.SparkArray.count
            tableempty = false
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(!tableempty)
        {
        var cell: SparkTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "SparkTableViewCell", bundle: nil), forCellReuseIdentifier: "SparkTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
            
        }
        cell?.delegate = self
        cell?.tag = indexPath.row
        //    cell?.sparkdescription?.sizeToFit()
        //    cell?.updateConstraintsIfNeeded()
        return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
            
            cell.textLabel?.text = "No result !"
            cell.textLabel?.textColor = UIColor.darkGray
            return cell
        }
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(!tableempty)
        {
        
        if let sparkCell = cell as? SparkTableViewCell  {
            
            sparkCell.configureCell(Spark: self.SparkArray[indexPath.row])
            
        }
        }
        
        
    }
    
  
    func didtapviewbutton(Spark: Spark, Cell: UITableViewCell) {
        print("here")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
        SparkdetailsViewController.thisspark = Spark
        self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
        
    }
    
    func didtapjoinbutton(Spark: Spark, Cell: SparkTableViewCell, custom : Any?,sender:Any?) {
        print("here")
        if(custom != nil)
        {
            print("")
            if(custom as! String == "comments")
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let CommentViewController = storyBoard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
                CommentViewController.thisspark = Spark
                self.navigationController?.pushViewController(CommentViewController, animated: true)
            }
            if(custom as! String == "option")
            {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                var action_title : String = "Report"
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    action_title = "Edit"
                }
                //First action
                let actionOne = UIAlertAction(title: action_title, style: .default) { (action) in
                    if(Spark.userId == Auth.auth().currentUser?.uid )
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let UpdateSparkViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateSparkViewController") as! UpdateSparkViewController
                        UpdateSparkViewController.spark = Spark
                        self.navigationController?.pushViewController(UpdateSparkViewController, animated: true)
                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                        ReportViewController.reportitem = "Spark"
                        ReportViewController.reportitemid = Spark.sparkId!
                        self.navigationController?.pushViewController(ReportViewController, animated: true)
                    }
                }
                alert.addAction(actionOne)
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    print("delete")
                    let action_delete_title : String = "Delete"
                    let actiondelete = UIAlertAction(title: action_delete_title, style: .default) { (action) in
                        self.netService.deletespark(sparkId: Spark.sparkId!, completed: {
                            //code
                            self.SparkArray.remove(at: Cell.tag)
                            self.tableView.reloadData()
                        })
                    }
                    alert.addAction(actiondelete)
                }
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
                
                alert.addAction(actionThree)
                
                if let popoverController = alert.popoverPresentationController {
//                    popoverController.sourceView = self.view
//                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                    popoverController.permittedArrowDirections = []
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                }
                
                //Present alert
                self.present(alert, animated: true, completion: nil)
            }
            if(custom as! String == "share")
            {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                //First action
                //First action
                let actionOne = UIAlertAction(title: "Facebook", style: .default) { (action) in
                    DispatchQueue.main.async {
                        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                        
                        content.quote = (Spark.title?.uppercased())! + " : " + Spark.description!
                        content.contentURL = NSURL(string: "https://t6b9g.app.goo.gl/V9Hh")! as URL
                        
                        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
                    }
                }
                
                //Second action
                let actionTwo = UIAlertAction(title: "Twitter", style: .default) { (action) in
                    DispatchQueue.main.async {
                        //Checking if user is connected to Facebook
                        
                        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                            // App must have at least one logged-in user to compose a Tweet
                            let composer = TWTRComposer()
                            
                            composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                            //composer.setImage(UIImage(named: "twitterkit"))
                            
                            // Called from a UIViewController
                            
                            composer.show(from: self.navigationController!, completion: { (result) in
                                if (result == .done) {
                                    print("Successfully composed Tweet")
                                } else {
                                    print("Cancelled composing")
                                }
                            })
                            // self.present(composer, animated: true, completion: nil)
                        } else {
                            // Log in, and then check again
                            Twitter.sharedInstance().logIn { session, error in
                                if session != nil { // Log in succeeded
                                    let composer = TWTRComposer()
                                    
                                    composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                                    //composer.setImage(UIImage(named: "twitterkit"))
                                    
                                    // Called from a UIViewController
                                    
                                    composer.show(from: self.navigationController!, completion: { (result) in
                                        if (result == .done) {
                                            print("Successfully composed Tweet")
                                        } else {
                                            print("Cancelled composing")
                                        }
                                    })
                                } else {
                                    
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: "No Twitter Accounts Available Or Error in completing request")
                                }
                            }
                        }
                    }
                }
                
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
                alert.addAction(actionOne)
                alert.addAction(actionTwo)
                alert.addAction(actionThree)
                
                if let popoverController = alert.popoverPresentationController {
//                    popoverController.sourceView = self.view
//                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                    popoverController.permittedArrowDirections = []
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                }
                //Present alert
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func showAlert(service:String)
    {
        
        let alertView = SCLAlertView()
        alertView.showError("OOPS", subTitle: "You are not connected to \(service)")
    }
    
    func didtapprofilebutton(user : Useri)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let ProfileViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        ProfileViewController.thisuser = user
        ProfileViewController.navigationItem.leftBarButtonItem = nil
        ProfileViewController.navigationItem.rightBarButtonItem = nil
        self.navigationController?.pushViewController(ProfileViewController, animated: true)
    }
    
    
 
    
    
    
    
    
    
}

