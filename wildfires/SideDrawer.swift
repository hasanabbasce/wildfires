//
//  SideDrawer.swift
//  wildfires
//
//  Created by Hasan Abbas on 25/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit

class SideDrawer: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var catogories: [String] = ["Children's Rights","Immigration","World/International","Animal Rights", "Criminal Justice", "Education" ,"Economic Justice","Environment","Health and Safety","Human Rights","Politics","Racial Justice","Women","Other"]
    var selectedcatogories: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    

    
     func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         return self.catogories.count
    }
    
    
    
    // Make the background color show through
  
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
         cell.selectionStyle = .none
        cell.textLabel?.text = self.catogories[indexPath.row]
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected  \(catogories[indexPath.row])")
        
       
       /* let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let CatogorySparkViewController = storyBoard.instantiateViewController(withIdentifier: "CatogorySparkViewController") as! CatogorySparkViewController
        CatogorySparkViewController.category = catogories[indexPath.row]
        CatogorySparkViewController.navigationItem.leftBarButtonItem = nil
        CatogorySparkViewController.navigationItem.rightBarButtonItem = nil
        self.navigationController?.pushViewController(CatogorySparkViewController, animated: true)
        */
      /*  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let CatogorySparkViewController = storyboard.instantiateViewController(withIdentifier: "CatogorySparkViewController") as! CatogorySparkViewController //SWRevealViewController
        CatogorySparkViewController.category = catogories[indexPath.row]
        self.present(CatogorySparkViewController, animated: false, completion: nil)*/
        
        let CatogorySparkViewController = self.storyboard!.instantiateViewController(withIdentifier: "CatogorySparkViewController") as! CatogorySparkViewController
        
        CatogorySparkViewController.category = catogories[indexPath.row]
        let navController = UINavigationController(rootViewController: CatogorySparkViewController) 
        self.present(navController, animated:true, completion: nil)
        
    }
    
    
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("deselected  \(catogories[indexPath.row])")
        
        if let cell = tableView.cellForRow(at: indexPath) {
            //   cell.accessoryType = .none
        }
        
        if let sr = tableView.indexPathsForSelectedRows {
            print("didDeselectRowAtIndexPath selected rows:\(sr)")
        }
    }
    
   
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
