//
//  Customizable.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import Foundation
import UIKit


class circularimage: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radius: CGFloat = self.bounds.size.width / 2.0
        
        self.layer.cornerRadius = radius
    }
}

extension UIColor {
    convenience init(colorWithHexValue hex:String, alpha:CGFloat = 1.0){
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            self.init(
                red: CGFloat(1.0),
                green: CGFloat(1.0),
                blue: CGFloat(1.0),
                alpha: CGFloat(1.0)
            )
        }
        else{
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        }
    }
}

extension UIViewController {
    
    func isValidEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}




open class CustomLabel : UILabel {
    @IBInspectable open var characterSpacing:CGFloat = 1 {
        didSet {
            let attributedString = NSMutableAttributedString(string: self.text!)
            attributedString.addAttribute(NSKernAttributeName, value: self.characterSpacing, range: NSRange(location: 0, length: attributedString.length))
            self.attributedText = attributedString
        }
        
    }
}
 class ShadowUIView: UIView {
    let gradientLayer = CAGradientLayer()
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        
       // let shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.5
       // self.layer.shadowPath = shadowPath
         self.layer.shadowRadius = 10
        //self.layer.shouldRasterize = true
        //self.layer.rasterizationScale = UIScreen.main.scale
        if(self.restorationIdentifier == "navbar")
        {
       //  self.backgroundColor = UIColor(patternImage: UIImage(named: "appBarBg")!)
            
            
            gradientLayer.frame = self.frame
            
            gradientLayer.colors = [UIColor.init(colorWithHexValue: "#B5B9E6").cgColor, UIColor.init(colorWithHexValue: "#80E0FB").cgColor]
            gradientLayer.startPoint = CGPoint.init(x: 0, y: 1)
            gradientLayer.endPoint = CGPoint.init(x: 0.7, y: 1)
            self.layer.insertSublayer(gradientLayer, at: 0)
            
        }
        
        if(self.restorationIdentifier == "sidebarheader")
        {
            //  self.backgroundColor = UIColor(patternImage: UIImage(named: "appBarBg")!)
            
            
            gradientLayer.frame = self.frame
            
            gradientLayer.colors = [UIColor.init(colorWithHexValue: "#B5B9E6").cgColor, UIColor.init(colorWithHexValue: "#80E0FB").cgColor]
            gradientLayer.startPoint = CGPoint.init(x: 0, y: 1)
            gradientLayer.endPoint = CGPoint.init(x: 0.5, y: 1)
            self.layer.insertSublayer(gradientLayer, at: 0)
            
        }
        
    }
    override func layoutSubviews() {
        
            gradientLayer.frame = self.layer.bounds;
       
    }
    
   
    
}


@IBDesignable class CustomizableImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowOpacity = 0.5
            layer.shadowRadius = shadowRadius
        }
    }
    
 
    
   
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
}

@IBDesignable class CustomizableButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
    
    
}

@IBDesignable class CustomizableTextfield: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
}
