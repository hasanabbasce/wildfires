
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Notificationsbk {
    
    var title : String?
    var message : String?
    var currentUserId : String?
    var email : String?
    
    var userimg : String?
    var fcmToken : String?
    var userid : String?
    var identifier : String?
    var nodeid : String?
    
    
    
    init(snapshot: DataSnapshot){
        self.title = (snapshot.value as! NSDictionary)["title"] as? String ?? ""
        self.message = (snapshot.value as! NSDictionary)["message"] as? String ?? ""
        self.currentUserId = (snapshot.value as! NSDictionary)["currentUserId"] as? String
        self.email = (snapshot.value as! NSDictionary)["email"] as? String ?? ""
        
        self.userimg = (snapshot.value as! NSDictionary)["userimg"] as? String ?? ""
        self.fcmToken = (snapshot.value as! NSDictionary)["fcmToken"] as? String ?? ""
        self.userid = (snapshot.value as! NSDictionary)["userid"] as? String ?? ""
        
        self.identifier = (snapshot.value as! NSDictionary)["identifier"] as? String ?? ""
        self.nodeid = (snapshot.value as! NSDictionary)["nodeid"] as? String ?? ""

    }
    
    init( title: String, message: String, fcmToken: String ,identifier: String,nodeid: String){
        
        self.title = title
        self.message = message
        self.fcmToken = fcmToken
        self.identifier = identifier
        self.nodeid = nodeid
        
    }
    
    
    
    init(initdic: [String: Any]){
        
        if(initdic["title"] != nil)
        {
            self.title = initdic["title"]! as? String
        }
        if(initdic["message"] != nil)
        {
            self.message = initdic["message"]!  as? String
        }
        if(initdic["currentUserId"] != nil)
        {
            self.currentUserId = initdic["currentUserId"]! as? String
        }
        if(initdic["email"] != nil)
        {
            self.email = initdic["email"]! as? String
        }
        if(initdic["userimg"] != nil)
        {
            self.userimg = initdic["userimg"]! as? String
        }
        
        if(initdic["fcmToken"] != nil)
        {
            self.fcmToken = initdic["fcmToken"]! as? String
        }
        if(initdic["userid"] != nil)
        {
            self.userid = initdic["userid"]! as? String
        }
        if(initdic["identifier"] != nil)
        {
            self.identifier = initdic["identifier"]! as? String
        }
        if(initdic["nodeid"] != nil)
        {
            self.nodeid = initdic["nodeid"]! as? String
        }
        
        
    }
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.title != nil)
        {
            initdic["title"] = self.title!
        }
        if(self.message != nil)
        {
            initdic["message"] = self.message!
        }
        
        if(self.currentUserId != nil)
        {
            initdic["currentUserId"] = self.currentUserId!
        }
        if(self.email != nil)
        {
            initdic["email"] = self.email!
        }
        if(self.userimg != nil)
        {
            initdic["userimg"] = self.userimg!
        }
        
        if(self.fcmToken != nil)
        {
            initdic["fcmToken"] = self.fcmToken!
        }
        if(self.userid != nil)
        {
            initdic["userid"] = self.userid!
        }
        if(self.identifier != nil)
        {
            initdic["identifier"] = self.identifier!
        }
        if(self.nodeid != nil)
        {
            initdic["nodeid"] = self.nodeid!
        }
        
        
        return initdic
    }
    
    
    
}

