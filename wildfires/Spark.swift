
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Spark {
    
    var sparkId: String?
    var userId: String?
    var title: String?
    var cover: String?
    var description: String?
    var catogories: String?
    var username: String?
    var profileptictureUrl: String?
    var SparkUser: Useri?
    
    var location: String?
    var lat: Double?
    var lon: Double?
    var country: String?
    var state: String?
    var city: String?
    var postal_code: String?
    var route: String?
    var joinstatus: Bool = false
    var heartstatus: Bool = false
    var postDate: NSNumber?
    var ref: DatabaseReference?
    var key: String = ""
    var tags: [String]?
    
    var commentcount: Int?
    var joincount: Int?
    var heartcount: Int?
    
    
    
    init(snapshot: DataSnapshot){
        
        self.sparkId = (snapshot.value as! NSDictionary)["sparkId"] as? String
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.title = (snapshot.value as! NSDictionary)["title"] as? String
        self.cover = (snapshot.value as! NSDictionary)["cover"] as? String
        self.description = (snapshot.value as! NSDictionary)["description"] as? String
        self.catogories = (snapshot.value as! NSDictionary)["categories"] as? String
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double
        self.lon = (snapshot.value as! NSDictionary)["lon"] as? Double
        self.country = (snapshot.value as! NSDictionary)["country"] as? String
        self.state = (snapshot.value as! NSDictionary)["state"] as? String
        self.city = (snapshot.value as! NSDictionary)["city"] as? String
        self.postal_code = (snapshot.value as! NSDictionary)["postal_code"] as? String
        self.route = (snapshot.value as! NSDictionary)["route"] as? String

        self.commentcount = (snapshot.value as! NSDictionary)["commentcount"] as? Int ?? 0
        self.joincount = (snapshot.value as! NSDictionary)["joincount"] as? Int ?? 0
        self.heartcount = (snapshot.value as! NSDictionary)["heartcount"] as? Int ?? 0
        
        
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    
    init(sparkId: String, userId: String, title: String, cover: String ,description: String, catogories: String, location: String, postDate: NSNumber, country: String ,state: String ,city: String,postal_code: String,route: String,lat: Double,lon: Double,tags: [String]){
        
        self.sparkId = sparkId
        self.userId = userId
        self.title = title
        self.cover = cover
        self.description = description
        self.catogories = catogories
        
        self.location = location
        self.lat = lat
        self.lon = lon
        self.country = country
        self.state = state
        self.city = city
        self.postal_code = postal_code
        self.route = route
        self.tags = tags
        self.postDate = postDate
        self.ref = Database.database().reference()
        
    }
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["sparkId"] != nil)
        {
            self.sparkId = initdic["sparkId"]! as? String
        }
        if(initdic["userId"] != nil)
        {
            self.userId = initdic["userId"]! as? String
        }
        if(initdic["title"] != nil)
        {
            self.title = initdic["title"]!  as? String
        }
        if(initdic["cover"] != nil)
        {
            self.cover = initdic["cover"]! as? String
        }
        if(initdic["description"] != nil)
        {
            self.description = initdic["description"]! as? String
        }
        if(initdic["catogories"] != nil)
        {
            self.catogories = initdic["categories"]! as? String
        }
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lon = initdic["lon"]! as? Double
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["state"] != nil)
        {
            self.state = initdic["state"]! as? String
        }
        if(initdic["city"] != nil)
        {
            self.city = initdic["city"]! as? String
        }
        if(initdic["postal_code"] != nil)
        {
            self.postal_code = initdic["postal_code"]! as? String
        }
        if(initdic["route"] != nil)
        {
            self.route = initdic["route"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }

    public static func ==(lhs: Spark, rhs: Spark) -> Bool{
        return
            lhs.sparkId == rhs.sparkId
    }
    
    
    
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.sparkId != nil)
        {
            initdic["sparkId"] = self.sparkId!
        }
        if(self.userId != nil)
        {
            initdic["userId"] = self.userId!
        }
        if(self.title != nil)
        {
            initdic["title"] = self.title!
            initdic["title_lowercase"] = self.title!.lowercased()
        }
        
        if(self.cover != nil)
        {
            initdic["cover"] = self.cover!
        }
        if(self.description != nil)
        {
            initdic["description"] = self.description!
        }
        if(self.catogories != nil)
        {
            initdic["categories"] = self.catogories!
            var catogoriess = self.catogories!.components(separatedBy: ",")
            for var catogory in catogoriess {
                catogory = catogory.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
                initdic["category-"+catogory] = true
                
            }
        }
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lon != nil)
        {
            initdic["lon"] = self.lon!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.state != nil)
        {
            initdic["state"] = self.state!
        }
        
        if(self.city != nil)
        {
            initdic["city"] = self.city!
        }
        if(self.postal_code != nil)
        {
            initdic["postal_code"] = self.postal_code!
        }
        if(self.route != nil)
        {
            initdic["route"] = self.route!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        if(self.tags != nil)
        {
                                    for tag in self.tags!
                                    {
                                          initdic["tag-"+tag.lowercased()] = true
                                    }
        }
        
        
        
        return initdic
    }
    
    
    
    
    
    
}

