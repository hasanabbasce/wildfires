
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Comments {
    
    
    var userId: String?
    var username: String?
    var profileimage: String?
    var sparkId: String?
    var comment: String?
    var postDate: NSNumber?
    var ref: DatabaseReference?
    var key: String = ""
    
    
    
    
    init(snapshot: DataSnapshot){
        
        
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.sparkId = (snapshot.value as! NSDictionary)["sparkId"] as? String
        self.comment = (snapshot.value as! NSDictionary)["comment"] as? String
        self.username = (snapshot.value as! NSDictionary)["username"] as? String
        self.profileimage = (snapshot.value as! NSDictionary)["profileimage"] as? String
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    init(userId: String, sparkId: String, comment: String, postDate: NSNumber ){
        
        
        
        self.userId = userId
        self.sparkId = sparkId
        self.comment = comment
        self.postDate = postDate
        self.ref = Database.database().reference()
        
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["userId"] != nil)
        {
            self.userId = initdic["userId"]! as? String
        }
        if(initdic["sparkId"] != nil)
        {
            self.sparkId = initdic["sparkId"]! as? String
        }
        if(initdic["comment"] != nil)
        {
            self.comment = initdic["comment"]! as? String
        }
        if(initdic["username"] != nil)
        {
            self.username = initdic["username"]! as? String
        }
        if(initdic["profileimage"] != nil)
        {
            self.profileimage = initdic["profileimage"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
    }
    

    
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.userId != nil)
        {
            initdic["userId"] = self.userId!
        }
        if(self.sparkId != nil)
        {
            initdic["sparkId"] = self.sparkId!
        }
        if(self.comment != nil)
        {
            initdic["comment"] = self.comment!
        }
        if(self.username != nil)
        {
            initdic["username"] = self.username!
        }
        if(self.profileimage != nil)
        {
            initdic["profileimage"] = self.profileimage!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        
        
        return initdic
    }
    
    
    
    
    
}

