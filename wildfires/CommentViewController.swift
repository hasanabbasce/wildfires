//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import Firebase
import SCLAlertView

class CommentViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource,CommentTableViewCellDelegate {
    
    

    @IBOutlet weak var commentbox: UITextView!
    
    
    
    // var sparkid : String?
    var commentsArray = [Comments]()
    var thisspark : Spark?
    
    var netService = NetworkingService()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //sparkid = "6B341374-CBBF-45FE-81C6-EDD182080500"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        setTapGestureRecognizerOnView()
        commentbox!.layer.borderWidth = 1
        commentbox!.layer.borderColor = UIColor.lightGray.cgColor
       // commentbox.delegate = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.delegate = self
        tableView.dataSource = self
        // self.tableView.rowHeight = UITableViewAutomaticDimension
        // self.tableView.estimatedRowHeight = 250
        rendercomments()
        
        
        self.commentbox.resolveHashTags()
        
    }
    
    
    
    
    private func rendercomments()
    {
        netService.fetchcommentsfromDB(Sparkid: (self.thisspark?.sparkId!)!) { (comments) in
            self.commentsArray = comments
            self.commentsArray.sort(by: { (comment1, comment2) -> Bool in
                Int(comment1.postDate!) > Int(comment2.postDate!)
            })
            
             self.commentsArray = self.netService.isblockedcomment(comments: self.commentsArray)
            self.tableView.reloadData()
        }
    }
    

    
    func showAlert(service:String)
    {
        
        let alertView = SCLAlertView()
        alertView.showError("OOPS", subTitle: "You are not connected to \(service)")
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    @IBAction func commentsubmit(_ sender: Any) {
        let postDate = NSDate().timeIntervalSince1970 as NSNumber
        let newComment = Comments(userId: (Auth.auth().currentUser?.uid)!, sparkId: (thisspark?.sparkId)!, comment: commentbox.text, postDate : postDate)
        self.netService.addcommentToDB(comment: newComment) {
            self.commentbox!.text = ""
        }
    }
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.commentsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: CommentTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell
            
        }
        cell?.delegate = self
        cell?.tag = indexPath.row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let commentstring = self.commentsArray[indexPath.row].comment!
        let heigt = commentstring.height(withConstrainedWidth: tableView.contentSize.width, font: UIFont.systemFont(ofSize: 15))
         print(heigt)
        return heigt+60
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if let CommentCell = cell as? CommentTableViewCell  {
            CommentCell.configureCell(Comment: self.commentsArray[indexPath.row])
            
        }
      
        
        
    }
    
    func eventtoview(comment: Comments,Cell : UITableViewCell,action : String,sender:Any?)
    {
        if(action == "setting")
        {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            var action_title : String = "Report"
            if(comment.userId == Auth.auth().currentUser?.uid )
            {
                action_title = "Delete"
            }
            
            let actiondelete = UIAlertAction(title: action_title, style: .default) { (action) in
                if(comment.userId == Auth.auth().currentUser?.uid )
                {
                    self.netService.deletecomment(commentid: comment.key,sparkId:comment.sparkId!, completed: {
                        //code
                        self.commentsArray.remove(at: Cell.tag)
                        self.tableView.reloadData()
                    })
                }
                else
                {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                    ReportViewController.reportitem = "Comment"
                    ReportViewController.reportitemid = comment.key
                    self.navigationController?.pushViewController(ReportViewController, animated: true)
                }
            }
            alert.addAction(actiondelete)
            
            
            let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            //Add action to action sheet
            
            alert.addAction(actionThree)
            
            if let popoverController = alert.popoverPresentationController {
                //            popoverController.sourceView = self.view
                //            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                //            popoverController.permittedArrowDirections = []
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = (sender as AnyObject).bounds;
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
            }
            //Present alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}


extension CommentViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        animateView(up: true, moveValue: 80)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateView(up: false, moveValue:
            80)
    }
    func textViewDidChange(_ textView: UITextView) {
        commentbox.resolveHashTags()
        
    }
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    
    
}
