
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Tags {
    
   
    var userId: String!
    var sparkId: String
    var hashtag: String
    var ref: DatabaseReference!
    var key: String = ""
    
    
    
    
    init(snapshot: DataSnapshot){
        
        
        self.userId = (snapshot.value as! NSDictionary)["userId"] as! String
        self.sparkId = (snapshot.value as! NSDictionary)["sparkId"] as! String
        self.hashtag = (snapshot.value as! NSDictionary)["hashtag"] as! String
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    
    init( userId: String, sparkId: String, hashtag: String ){
        
       
        self.userId = userId
        self.sparkId = sparkId
        self.hashtag = hashtag
        self.ref = Database.database().reference()
        
    }
    
    
    
    
    
    
    func toAnyObject() -> [String: Any]{
        return ["userId":self.userId,"sparkId":self.sparkId ,"hashtag":self.hashtag]
    }
    
    
    
    
    
    
}

