
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct JoinedSpark {
    
    var joinedsparkId: String?
    var userId: String?
    var sparkuserId: String?
    var sparkId: String?
    var status: Bool?
    var heartstatus: Bool?
    var ref: DatabaseReference?
    var key: String = ""
    
    
    
    
    init(snapshot: DataSnapshot){
        
        self.joinedsparkId = (snapshot.value as! NSDictionary)["joinedsparkId"] as? String
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.sparkuserId = (snapshot.value as! NSDictionary)["sparkuserId"] as? String
        self.sparkId = (snapshot.value as! NSDictionary)["sparkId"] as? String
        self.status = (snapshot.value as! NSDictionary)["joinedStatus"] as? Bool
        self.heartstatus = (snapshot.value as! NSDictionary)["heartStatus"] as? Bool
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    
    init(joinedsparkId: String, userId: String, sparkuserId: String, sparkId: String ,status : Bool,heartstatus : Bool){
        
        self.joinedsparkId = joinedsparkId
        self.userId = userId
        self.sparkuserId = sparkuserId
        self.sparkId = sparkId
        self.status = status
        self.heartstatus = heartstatus
        self.ref = Database.database().reference()
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.joinedsparkId != nil)
        {
            initdic["joinedsparkId"] = self.joinedsparkId!
        }
        
        if(self.userId != nil)
        {
            initdic["userId"] = self.userId!
        }
        
        if(self.sparkuserId != nil)
        {
            initdic["sparkuserId"] = self.sparkuserId!
        }
        
        if(self.sparkId != nil)
        {
            initdic["sparkId"] = self.sparkId!
        }
        
        if(self.status != nil)
        {
            initdic["joinedStatus"] = self.status!
        }
       
        if(self.heartstatus != nil)
        {
            initdic["heartStatus"] = self.heartstatus!
        }
        
        
        
        return initdic
        
        
    }
    
    
    
    
    
    
}

