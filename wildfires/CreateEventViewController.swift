//
//  CreateEventViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 19/02/2018.
//  Copyright © 2018 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView
import Firebase

class CreateEventViewController: UIViewController , UITextFieldDelegate , PlaceSearchResultsControllerDelegate{
    
    
    @IBOutlet weak var location: SkyFloatingLabelTextField!
    
    @IBOutlet weak var datetime: SkyFloatingLabelTextField!
    
    @IBOutlet weak var eventtitle: SkyFloatingLabelTextField!
    
    @IBOutlet weak var eventtime: SkyFloatingLabelTextField!
    
    
    
    var sparkId:String?
    
    var PlaceSearchResultsControllervar:PlaceSearchResultsController!
    
    let datePicker = UIDatePicker()
    
    let timePicker = UIDatePicker()
    
    let activityData = ActivityData.init(size: nil, message: "Loading ..", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    
    var netService = NetworkingService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      location.delegate = self
        rendercurrentlocation()
        PlaceSearchResultsControllervar = PlaceSearchResultsController()
        PlaceSearchResultsControllervar.delegate = self
        showDatePicker()
        // Do any additional setup after loading the view.
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: "donedatePicker")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: "cancelDatePicker")
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        datetime.inputAccessoryView = toolbar
        // add datepicker to textField
        datetime.inputView = datePicker
        
        
        
        
        //Formate Date
        timePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar2 = UIToolbar();
        toolbar2.sizeToFit()
        
        //done button & cancel button
        let doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: "donedatePicker2")
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton2 = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: "cancelDatePicker2")
        toolbar2.setItems([doneButton2,spaceButton2,cancelButton2], animated: false)
        
        // add toolbar to textField
        eventtime.inputAccessoryView = toolbar2
        // add datepicker to textField
        eventtime.inputView = timePicker
        
    }
    
    func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        datetime.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    
    func donedatePicker2(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        eventtime.text = formatter.string(from: timePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    func cancelDatePicker2(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }

    
    func rendercurrentlocation()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.location != nil )
        {
            location.text = appDelegate.location
        }
    }
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String,location_details : [ String : String ]) {
        
        self.location.text = title
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func createevent(_ sender: Any) {
        
        if (location?.text?.isEmpty)! || (datetime?.text?.isEmpty)! || (eventtitle?.text?.isEmpty)! || (eventtime?.text?.isEmpty)! {
        }
        else
        {
        let eventId = NSUUID().uuidString
        let postDate = NSDate().timeIntervalSince1970 as NSNumber
            let event = Event(eventId: eventId, sparkId: sparkId!, userId: (Auth.auth().currentUser?.uid)!, date: datePicker.date.timeIntervalSince1970 as NSNumber,time : eventtime.text! , description: eventtitle.text!, location: location.text!, postDate: postDate)
        netService.addevent(event: event) {
            self.navigationController?.popViewController(animated: true)
        }
            
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == location)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(PlaceSearchResultsControllervar, animated: false)
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
