//
//  ChatFunctions.swift
//  WhatsAppClone
//
//  Created by Frezy Stone Mboumba on 8/27/16.
//  Copyright © 2016 Frezy Stone Mboumba. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import SCLAlertView

struct ChatFunctions{
    
    var chatRoom_id =  String()
    
    fileprivate var databaseRef: DatabaseReference! {
        return Database.database().reference()
    }
    
    mutating func startChat(_ user1: Useri, user2: Useri){
        
        let userId1 = user1.uid
        let userId2 = user2.uid
        
        var chatRoomId = ""
        
        let comparison = userId1?.compare(userId2!).rawValue
        
        let members = [user1.fullname!, user2.fullname!]
        
        if comparison! < 0 {
            chatRoomId = userId1! + userId2!
        } else {
            chatRoomId = userId2! + userId1!
        }
        
        self.chatRoom_id = chatRoomId
        self.createChatRoomId(user1, user2: user2, members:members, chatRoomId: chatRoomId)
        
    }
    
    fileprivate func createChatRoomId(_ user1: Useri, user2: Useri, members:[String], chatRoomId: String){
        
        print(chatRoomId)
       // let chatRoomRef = databaseRef.child("ChatRooms").queryOrdered(byChild: "chatRoomId").queryEqual(toValue: chatRoomId)
            let chatRoomRef = databaseRef.child("ChatRooms").child(chatRoomId)
        
        chatRoomRef.observe(.value, with: { (snapshot) in
            
            var createChatRoom = true
            
            if snapshot.exists() {
                
                //for chatRoom in (snapshot.value! as AnyObject).allValues {
                    let chatRooms = ChatRoom(snapshot: snapshot)
                    
                    
                    if chatRooms.chatRoomId! == chatRoomId {
                        createChatRoom = false
                    }
                //}
            }
            
            if createChatRoom {
                
                self.createNewChatRoomId(user1.fullname!, other_Username: user2.fullname!, userId: user1.uid!, other_UserId: user2.uid!, members: members, chatRoomId: chatRoomId, lastMessage: "", userPhotoUrl: user1.profilePictureUrl!, other_UserPhotoUrl: user2.profilePictureUrl!,date: NSDate().timeIntervalSince1970 as NSNumber)
            }
            
            
            
        }) { (error) in
            
            DispatchQueue.main.async(execute: {
                let alertView =  SCLAlertView()
                alertView.showError("😁OOPS😁", subTitle: error.localizedDescription)
            })        }
        
        
    }
    
    
    fileprivate func createNewChatRoomId(_ username: String, other_Username: String,userId: String,other_UserId: String,members: [String],chatRoomId: String,lastMessage: String,userPhotoUrl: String,other_UserPhotoUrl: String, date: NSNumber){
        
        let newChatRoom = ChatRoom(username: username, other_Username: other_Username, userId: userId, other_UserId: other_UserId, members: members, chatRoomId: chatRoomId, lastMessage: lastMessage, userPhotoUrl: userPhotoUrl, other_UserPhotoUrl: other_UserPhotoUrl,date:date)
        
        let chatRoomRef = databaseRef.child("ChatRooms").child(chatRoomId)
        chatRoomRef.setValue(newChatRoom.toAnyObject())
        
    }
    
    
    
    
}
