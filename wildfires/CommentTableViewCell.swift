//
//  CommentTableViewCell.swift
//  wildfires
//
//  Created by Hasan Abbas on 31/08/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate {
    func eventtoview(comment: Comments,Cell : UITableViewCell,action : String, sender : Any?)
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var profileimage: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var comment: UILabel!
    
    @IBOutlet weak var postdate: UILabel!
    
    
    
    var delegate: CommentTableViewCellDelegate!
    
    var Comment : Comments?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func configureCell(Comment: Comments){
        self.Comment = Comment
        self.profileimage?.sd_setImage(with: URL(string: Comment.profileimage!))
        self.username.text = Comment.username
        self.comment.text = Comment.comment
        self.comment.resolveHashTags()
        
        let fromDate = NSDate(timeIntervalSince1970: TimeInterval((self.Comment?.postDate!)!))
        let toDate = NSDate()
        
        postdate.text = timeAgoSinceDate(fromDate as Date, currentDate: toDate as Date, numericDates: true)
        

    }
    
    @IBAction func setting(_ sender: Any) {
        self.delegate.eventtoview(comment: self.Comment!, Cell: self, action: "setting",sender:sender)
    }
    
    
}
