//
//  SearchResultsController.swift
//  PlacesLookup
//
//  Created by Malek T. on 9/30/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit
import GooglePlaces
import NVActivityIndicatorView
import SCLAlertView
import Firebase
import FBSDKShareKit

var networkingService = NetworkingService()


class FeedsViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource  , SparkTableViewCellDelegate  , CLLocationManagerDelegate {
    
    
    // MARK: - Properties
     var hit:Bool=true
     var SparkArray: [Spark] = []
     var runtimeSparkArray: [Spark] = []
    var paginateSparkArray: [Spark] = []
    var paginatedateSparkArray: [NSNumber : [Spark]]  = [ : ]
     var last_date:NSNumber=0
     var limit:Int=5
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sidebaropen: UIBarButtonItem!
    let postDate = NSDate().timeIntervalSince1970 as NSNumber
    var netService = NetworkingService()
    let activityData = ActivityData.init(size: nil, message: "Creating Profile", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    // MARK: - View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
         
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.myinfo == nil)
        {
            appDelegate.rendercurrentuser()
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        
        sideMenu()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 250
        fetchAllSparks()
       // tableView.activityIndicatorView.startAnimating()
        
        
       
    }
    
    
    
    func sideMenu()
    {
        
        if revealViewController() != nil {
            sidebaropen.target=revealViewController()
            sidebaropen.action = #selector(SWRevealViewController.revealToggle(animated:))
            revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width * 0.85
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    


    
    private func fetchAllSparks(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading .. ")
        last_date = postDate
        fetchpaginate()
        
        netService.fetchAllSparks(date: postDate) {(sparks ) in
            self.runtimeSparkArray = sparks
            self.runtimeSparkArray.sort(by: { (spark1, spark2) -> Bool in
                
                Int(spark1.postDate!) > Int(spark2.postDate!)
            })
            self.SparkArray = self.runtimeSparkArray + self.paginateSparkArray
            
            self.SparkArray = self.netService.isblockedspark(sparks: self.SparkArray)
            self.tableView.reloadData()
        }
    }
    
    private func fetchpaginate(){
        
        
        netService.fetchAllSparkspaginate(date: last_date, limit: limit , completion: { ( sparks: [Spark] , date : NSNumber ) in
            
            if(sparks.count > 0)
            {
            self.paginateSparkArray = []
            self.paginatedateSparkArray[date] = sparks
            for item in self.paginatedateSparkArray {
                    self.paginateSparkArray.append(contentsOf: item.value)
            }
            
            self.paginateSparkArray.sort(by: { (spark1, spark2) -> Bool in
                Int(spark1.postDate!) > Int(spark2.postDate!)
            })
            
            self.hit=true
            self.SparkArray = self.runtimeSparkArray + self.paginateSparkArray
            self.last_date = self.paginateSparkArray[self.paginateSparkArray.count-1].postDate!
               self.SparkArray = self.netService.isblockedspark(sparks: self.SparkArray)
            self.tableView.reloadData()
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Enjoy !")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            else
            {
            self.hit = false
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Enjoy !")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        self.SparkArray = self.netService.isblockedspark(sparks: self.SparkArray)
        self.tableView.reloadData()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.SparkArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: SparkTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
        if cell == nil {
            
            tableView.register(UINib(nibName: "SparkTableViewCell", bundle: nil), forCellReuseIdentifier: "SparkTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
           
           
        }
       cell?.delegate = self
        cell?.tag = indexPath.row
    //    cell?.sparkdescription?.sizeToFit()
     //   cell?.updateConstraintsIfNeeded()
        return cell!

    }
   
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if let sparkCell = cell as? SparkTableViewCell  {
           sparkCell.configureCell(Spark: self.SparkArray[indexPath.row])
          
          
        }
        let row_id:Int = indexPath.row
       
        if(self.SparkArray.count == row_id+3)
        {
            
            
            if( hit)
            {
                
                hit=false
                fetchpaginate()
            }
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 )
        {
        return 8
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func didtapviewbutton(Spark: Spark, Cell: UITableViewCell) {
        print("here")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
        SparkdetailsViewController.thisspark = Spark
        self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
    }
    
    func didtapjoinbutton(Spark: Spark, Cell: SparkTableViewCell, custom : Any?,sender:Any?) {
       print("here")
        print(Cell.tag)
        
        if(custom != nil)
        {
            print("")
            if(custom as! String == "comments")
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let CommentViewController = storyBoard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
                CommentViewController.thisspark = Spark
                self.navigationController?.pushViewController(CommentViewController, animated: true)
            }
            if(custom as! String == "option")
            {
                
                
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                var action_title : String = "Report"
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    action_title = "Edit"
                }
                //First action
                let actionOne = UIAlertAction(title: action_title, style: .default) { (action) in
                    if(Spark.userId == Auth.auth().currentUser?.uid )
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let UpdateSparkViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateSparkViewController") as! UpdateSparkViewController
                        UpdateSparkViewController.spark = Spark
                        self.navigationController?.pushViewController(UpdateSparkViewController, animated: true)
                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                        ReportViewController.reportitem = "Spark"
                        ReportViewController.reportitemid = Spark.sparkId!
                        self.navigationController?.pushViewController(ReportViewController, animated: true)
                    }
                }
                alert.addAction(actionOne)
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    print("delete")
                    let action_delete_title : String = "Delete"
                    let actiondelete = UIAlertAction(title: action_delete_title, style: .default) { (action) in
                        self.netService.deletespark(sparkId: Spark.sparkId!, completed: {
                            //code
                            //array.remove(at: Index)
                        })
                    }
                    alert.addAction(actiondelete)
                }
               
                
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
                
                alert.addAction(actionThree)
                
                //Present alert
                
                if let popoverController = alert.popoverPresentationController {
//                    popoverController.sourceView = self.view
//                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                    popoverController.permittedArrowDirections = []
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                    //popoverController.modalPresentationStyle = .overFullScreen
                }
                self.present(alert, animated: true, completion: nil)
                
                
                
            
            }
            if(custom as! String == "share")
            {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                //First action
                let actionOne = UIAlertAction(title: "Facebook", style: .default) { (action) in
                    DispatchQueue.main.async {
                        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                       
                        content.quote = (Spark.title?.uppercased())! + " : " + Spark.description!
                        content.contentURL = NSURL(string: "https://t6b9g.app.goo.gl/V9Hh")! as URL

                        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
                    }
                }
                
                //Second action
                let actionTwo = UIAlertAction(title: "Twitter", style: .default) { (action) in
                    DispatchQueue.main.async {
                        //Checking if user is connected to Facebook

                        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                            // App must have at least one logged-in user to compose a Tweet
                            let composer = TWTRComposer()
                            
                            composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                            //composer.setImage(UIImage(named: "twitterkit"))
                            
                            // Called from a UIViewController
                  
                            composer.show(from: self.navigationController!, completion: { (result) in
                                if (result == .done) {
                                    print("Successfully composed Tweet")
                                } else {
                                    print("Cancelled composing")
                                }
                            })
                           // self.present(composer, animated: true, completion: nil)
                        } else {
                            // Log in, and then check again
                            Twitter.sharedInstance().logIn { session, error in
                                if session != nil { // Log in succeeded
                                    let composer = TWTRComposer()
                                    
                                    composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                                    //composer.setImage(UIImage(named: "twitterkit"))
                                    
                                    // Called from a UIViewController
                                    
                                    composer.show(from: self.navigationController!, completion: { (result) in
                                        if (result == .done) {
                                            print("Successfully composed Tweet")
                                        } else {
                                            print("Cancelled composing")
                                        }
                                    })
                                } else {
                                   
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: "No Twitter Accounts Available Or Error in completing request")
                                }
                            }
                        }
                    }
                }
                
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
                alert.addAction(actionOne)
                alert.addAction(actionTwo)
                alert.addAction(actionThree)
                
                if let popoverController = alert.popoverPresentationController {
//                    popoverController.sourceView = self.view
//                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                    popoverController.permittedArrowDirections = []
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                }
                //Present alert
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func showAlert(service:String)
    {
        
        let alertView = SCLAlertView()
        alertView.showError("OOPS", subTitle: "You are not connected to \(service)")
    }
    
    func didtapprofilebutton(user : Useri)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let ProfileViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        ProfileViewController.thisuser = user
        ProfileViewController.navigationItem.leftBarButtonItem = nil
        ProfileViewController.navigationItem.rightBarButtonItem = nil
        self.navigationController?.pushViewController(ProfileViewController, animated: true)
    }
    
}
