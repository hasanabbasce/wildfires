//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation


class DiscoverViewController: UIViewController,UISearchBarDelegate, PlaceSearchResultsControllerDelegate ,CLLocationManagerDelegate  , GMSMapViewDelegate {
  

    
var networkingService = NetworkingService()
   
    @IBOutlet weak var mapView: GMSMapView!
    
      var PlaceSearchResultsControllervar:PlaceSearchResultsController!
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
   var marker = GMSMarker()
    var sparkmarkers = [GMSMarker]()
   
    var circ : GMSCircle?
    var radius : Float = 10
    var locationhere : Bool = false
    var resultsArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview: (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
         createSearchBar()
         mapView.mapType = GMSMapViewType(rawValue: 1)!
        PlaceSearchResultsControllervar = PlaceSearchResultsController()
        PlaceSearchResultsControllervar.delegate = self
        self.mapView?.isMyLocationEnabled = true
        self.mapView?.delegate = self
        //Location Manager code to fetch current location
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }

        
        
        
       
        
        
        

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            print("denied")
            // Permission denied, do something else
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (locations.first != nil && locationhere == false )  {
            currentLocation = locations[0] as CLLocation
            locationhere = true
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
            locationManager.stopUpdatingLocation()
            
            DispatchQueue.main.async { () -> Void in
                let location = locations.last
                
                
                let newCoor = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                let newCoorCam = GMSCameraUpdate.setTarget(newCoor)
                self.mapView?.animate(with: newCoorCam)
                
                let camera  = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 12)
                //self.mapView.camera = camera
                self.mapView?.animate(to: camera)
                self.locationManager.stopUpdatingLocation()
                //Finally stop updating location otherwise it will come again and again in this delegate
                self.marker.position = camera.target
                self.marker.snippet = "Current location"
                self.marker.map = self.mapView
                
                if(self.circ != nil)
                {
                    self.circ?.map = nil
                }
                self.circ = GMSCircle(position: camera.target, radius: CLLocationDistance(self.radius*1000))
                self.circ?.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
                self.circ?.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
                self.circ?.strokeWidth = 2.5
                self.circ?.map = self.mapView
                self.networkingService.fetchSparknearby(currentLocation:  CLLocation(latitude: newCoor.latitude, longitude: newCoor.longitude), radius: Int(self.radius), completion: { (Sparks) in
                    let filteredSparks = self.networkingService.isblockedspark(sparks: Sparks)
                    self.showmarkersSparks(Sparks: filteredSparks)
                })
                
            }

            
            
//            CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
//
//                if (error != nil) {
//                    print("Reverse geocoder failed with error" + error!.localizedDescription)
//                    return
//                }
//
//                if placemarks!.count > 0 {
//                    let pm = placemarks![0]
//                    self.displayLocationInfo(pm)
//                } else {
//                    print("Problem with the data received from geocoder")
//                }
//            })
        }
    }
    
   
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
        locationManager.stopUpdatingLocation()
    }
    
    

    
    func createSearchBar()
    {
       
        
        let searchbar = UISearchBar()
        searchbar.delegate = self
        searchbar.placeholder = "Search"
        self.navigationItem.titleView = searchbar
        
        
        for subView in searchbar.subviews {
            
            for subViewOne in subView.subviews {
                
                if let textField = subViewOne as? UITextField {
                    subViewOne.backgroundColor = UIColor(white: 1, alpha: 0.2)
                    
                    //use the code below if you want to change the color of placeholder
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = UIColor.lightGray
                }
            }
        }
    }
    
    
 
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        let navigator = navigationController
        navigator?.pushViewController(PlaceSearchResultsControllervar, animated: false)
        return false
    }
    
    
    
    
    
    

    
    
   
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String ,location_details : [ String : String ]) {
        locationhere = false
        DispatchQueue.main.async { () -> Void in
            let newCoor = CLLocationCoordinate2D(latitude: (lat), longitude: (lon))
            let newCoorCam = GMSCameraUpdate.setTarget(newCoor)
            self.mapView?.animate(with: newCoorCam)
            let camera  = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 12)
            self.mapView?.animate(to: camera)
         //   self.searchlable.text = titlew
            self.marker.position = camera.target
            self.marker.snippet = title
             self.marker.map = self.mapView
            if(self.circ != nil)
            {
                self.circ?.map = nil
            }
            self.circ = GMSCircle(position: camera.target, radius: CLLocationDistance(self.radius*1000))
            self.circ?.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
            self.circ?.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
            self.circ?.strokeWidth = 2.5
            self.circ?.map = self.mapView
            self.networkingService.fetchSparknearby(currentLocation:  CLLocation(latitude: lat, longitude: lon), radius: Int(self.radius), completion: { (Sparks) in
                 let filteredSparks = self.networkingService.isblockedspark(sparks: Sparks)
                self.showmarkersSparks(Sparks: filteredSparks)
            })
            
        }
    }
    
    func showmarkersSparks(Sparks : [Spark])
    {
        for sparkmarker in sparkmarkers {
            sparkmarker.map = nil
        }
        sparkmarkers = []
        for spark in Sparks {
        let sparkmarker = GMSMarker()
        sparkmarker.position = CLLocationCoordinate2D(latitude: spark.lat!, longitude: spark.lon!)
        sparkmarker.snippet = spark.title
        sparkmarker.map = self.mapView
            let fire = UIImage(named: "wildfire-login-icon")!
            
            let markerView = UIImageView(image: fire)
            markerView.frame.size = CGSize(width: 80, height: 80)
            markerView.contentMode = .scaleAspectFit;
            markerView.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
            sparkmarker.iconView = markerView
            sparkmarker.userData = spark
         sparkmarkers.append(sparkmarker)
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if(marker.userData != nil)
        {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
        SparkdetailsViewController.thisspark = marker.userData as? Spark
        self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
        }
        return true
    }
    
    
    @IBAction func currentlocation(_ sender: Any) {
        self.locationManager.startUpdatingLocation()
    }
    
  
    
 
    
   
    
}



