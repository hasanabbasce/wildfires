//
//  SideDrawer.swift
//  wildfires
//
//  Created by Hasan Abbas on 25/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit


class SettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
     var netService = NetworkingService()
    
    var settingoptions: [String] = ["Reset Password","Help Center", "Report a Problem" ,"Privacy Policy","Terms","Log Out"]
    var selectedcatogories: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.settingoptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3
    }
    
    // Make the background color show through
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        //cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = self.settingoptions[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected  \(settingoptions[indexPath.row])")
        
       if (self.settingoptions[indexPath.row] == "Log Out")
       {
        netService.logOut {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.myinfo = nil
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Signin") as! Signin
            loginVC.modalTransitionStyle = .crossDissolve
            self.present(loginVC, animated: true, completion: nil)
            
        }
        }
        if (self.settingoptions[indexPath.row] == "Help Center")
        {
            guard let url = URL(string: "http://wildfiresconnect.com/index.php/contact-us-2/") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        if (self.settingoptions[indexPath.row] == "Privacy Policy")
        {
            guard let url = URL(string: "http://wildfiresconnect.com/index.php/privacy-policy/") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        if (self.settingoptions[indexPath.row] == "Terms")
        {
            guard let url = URL(string: "http://wildfiresconnect.com/index.php/terms-conditions/") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        if (self.settingoptions[indexPath.row] == "Report a Problem")
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        
            self.navigationController?.pushViewController(ReportViewController, animated: true)
        }
        
        
        
        if (self.settingoptions[indexPath.row] == "Reset Password")
        {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Reset Password", message: "Enter Your Email", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            //print("Text field: \(String(describing: textField?.text))")
            
            self.netService.forgotpassword(email: (textField?.text)!)
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
        }
        
        
        
        
        
        
        
        
        
    }
    
    
 
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
