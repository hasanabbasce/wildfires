//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class Signup: UIViewController , UITextFieldDelegate , UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate{
    
    @IBOutlet weak var fullnamefield: SkyFloatingLabelTextField!
    @IBOutlet weak var emailfield: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var confimpassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var profileImageView: circularimage!
     var networkingService = NetworkingService()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        setTapGestureRecognizerOnView()
        setSwipeGestureRecognizerOnView()
        assignbackground()
        addfields()
    }
    
    
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func addfields()
    {
        
        if let fullnamefield = fullnamefield {
            fullnamefield.placeholder = "FULL NAME"
            fullnamefield.title = "FULL NAME"
            
            fullnamefield.delegate = self
        }
        
        
        if let emailfield = emailfield {
            emailfield.placeholder = "EMAIL"
            emailfield.title = "EMAIL"
            emailfield.errorColor = UIColor.red
            emailfield.delegate = self
           
        }
        
        
        if let passwordField = passwordField {
            passwordField.placeholder = "PASSWORD"
            passwordField.title = "PASSWORD"
               passwordField.delegate = self
            
        }
        if let confimpassword = confimpassword {
            confimpassword.placeholder = "CONFIRM PASSWORD"
            confimpassword.title = "CONFIRM PASSWORD"
            confimpassword.delegate = self
            
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
                if(floatingLabelTextField.placeholder == "EMAIL")
                {
                if(text.characters.count < 3 || !text.contains("@")) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
                }
            }
        }
        return true
    }
    
    func assignbackground(){
        let background = UIImage(named: "bg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    @IBAction func chooseprofileimage(_ sender: Any) {
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = profileImageView
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
          //  popoverController.sourceView = self.view
          //  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          //  popoverController.permittedArrowDirections = []
              popoverController.sourceView = sender as? UIView
              popoverController.sourceRect = (sender as AnyObject).bounds;
              popoverController.permittedArrowDirections = UIPopoverArrowDirection.left
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func createAccount(_ sender: Any) {
        let email = emailfield?.text!.lowercased()
        let finalEmail = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = passwordField?.text!
        let reenterPassword = confimpassword?.text!
        let fullname = fullnamefield?.text!
        
        let data = UIImageJPEGRepresentation(profileImageView.image!, 0.2)!
        
        
        
        if (finalEmail?.isEmpty)! || (password?.isEmpty)! || (fullname?.isEmpty)!  {
            print("empty fields")
        }else {
            
            if password == reenterPassword {
                
                if isValidEmail(email: finalEmail!) {
                    self.networkingService.signUp(fullname: fullname!,email: finalEmail!, pictureData: data, password: password!)
                }
                
            } else {
                
                print("Passwords do not match. Please try again.")
            }
            
        }
        
        self.view.endEditing(true)
        
    

    }
    
    @IBAction func termscondition(_ sender: Any) {
        guard let url = URL(string: "http://wildfiresconnect.com/index.php/terms-conditions/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    
}


extension Signup {
    
    
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profileImageView.image = chosenImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fullnamefield?.resignFirstResponder()
        emailfield?.resignFirstResponder()
        passwordField?.resignFirstResponder()
        confimpassword?.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateView(up: true, moveValue: 80)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateView(up: false, moveValue:
            80)
    }
    
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    func setSwipeGestureRecognizerOnView(){
        let swipDown = UISwipeGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        swipDown.direction = .down
        self.view.addGestureRecognizer(swipDown)
    }
}

