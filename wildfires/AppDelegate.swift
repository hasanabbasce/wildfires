//
//  AppDelegate.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import GoogleSignIn
import NVActivityIndicatorView
import SCLAlertView
import UserNotifications
import FirebaseMessaging


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , GIDSignInDelegate , CLLocationManagerDelegate , MessagingDelegate {

    var window: UIWindow?
    var databaseRef: DatabaseReference! {
        
        return Database.database().reference()
    }
     var networkingService = NetworkingService()
    
     let activityData = ActivityData.init(size: nil, message: "Loading ..", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    
     let googleMapsApiKey = "AIzaSyB79B1EYs38CwZgrdQgF18Fm4LzKZzgwqo"
     var myinfo : Useri?
     var locationManager = CLLocationManager()
    
    var location: String?
    var lat: Double?
    var lon: Double?
    var country: String?
    var state: String?
    var city: String?
    var postal_code: String?
    var route: String?
    
    override init(){
        super.init()
        
        
        print(NSDate().timeIntervalSince1970)
        print(NSDate())
        
        
        GMSServices.provideAPIKey(googleMapsApiKey)
        GMSPlacesClient.provideAPIKey(googleMapsApiKey)
    }
    
    func rendercurrentuser()
    {
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        //self.networkingService.logOut {}
        if Auth.auth().currentUser != nil {
        networkingService.fetchCurrentUser { (user) in
            if let user = user {
                self.myinfo = user
                
                if(self.myinfo == nil)
                {
                    self.networkingService.logOut {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "Signin") as! Signin
                        self.window?.rootViewController = homeVC
                    }
                }
                else if(self.myinfo?.location == nil || self.myinfo?.location == "")
                {
                    self.myinfo?.location = self.location
                    self.myinfo?.country = self.country
                    self.myinfo?.state = self.state
                    self.myinfo?.city = self.city
                    self.myinfo?.postal_code = self.postal_code
                    self.myinfo?.route = self.route
                    self.myinfo?.lat = self.lat
                    self.myinfo?.lon = self.lon
                    self.networkingService.updateUserstringinfo(user: self.myinfo, general : true)
                    
                }
            }
            else
            {
                self.networkingService.logOut {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "Signin") as! Signin
                    self.window?.rootViewController = homeVC
                }
            }
        }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         self.locationManager.stopUpdatingLocation()
        DispatchQueue.main.async { () -> Void in
            let locationlast = locations.last
           
            
            let newCoor = CLLocationCoordinate2D(latitude: (locationlast?.coordinate.latitude)!, longitude: (locationlast?.coordinate.longitude)!)
            
            
            
            if let correctedAddress = "\(newCoor.latitude) , \(newCoor.longitude)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(correctedAddress)") {
                print(url)
                
                
                let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
                    // 3
                    do {
                        if data != nil{
                            let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                            
                            let results = dic["results"] as! NSArray
                            var location_details = [ String : String ]()
                            var iter = 0
                            for result in results {
                                let strObj = result as! NSDictionary
                                let geometry = strObj["geometry"] as! NSDictionary
                                
                                let locationgeo = geometry["location"] as! NSDictionary
                                self.lat = locationgeo["lat"] as! NSNumber as? Double
                                self.lon = locationgeo["lng"] as! NSNumber as? Double
                                if(iter < 2)
                                {
                                    self.location = strObj["formatted_address"] as? String
                                }
                                iter += 1
                                
                                for address in strObj["address_components"] as! NSArray
                                {
                                    
                                    let addressme = address as! NSDictionary
                                    
                                    let addresstypes : NSArray = addressme["types"] as! NSArray
                                    
                                    
                                    
                                    
                                    if (String(describing: addresstypes[0]) == "country") {
                                        location_details["country"] =  addressme["long_name"] as? String;
                                         self.country =  location_details["country"]
                                       
                                    }
                                    if (String(describing: addresstypes[0]) == "administrative_area_level_1") {
                                        location_details["state"] =  addressme["long_name"] as? String
                                        self.state = location_details["state"]!
                                    }
                                    if (String(describing: addresstypes[0]) == "administrative_area_level_2") {
                                        location_details["country"] =  addressme["long_name"] as? String
                                        self.country = location_details["country"]!
                                    }
                                    if (String(describing: addresstypes[0]) == "locality") {
                                        location_details["city"] =  addressme["long_name"] as? String
                                        self.city = location_details["city"]!
                                    }
                                    if (String(describing: addresstypes[0]) == "postal_code") {
                                        location_details["postal_code"] =  addressme["long_name"] as? String
                                        self.postal_code =  location_details["postal_code"]!
                                    }
                                    if (String(describing: addresstypes[0]) == "route") {
                                        location_details["route"] =  addressme["long_name"] as? String
                                        self.route = location_details["route"]!
                                    }
                                    
                                    
                                }
                                
                                
                               
                                
                                
                                
                                
                            }
                            
                            if(self.myinfo != nil && ( self.myinfo?.location == nil || self.myinfo?.location == "" ) )
                            {
                                self.myinfo?.location = self.location
                                self.myinfo?.country = self.country
                                self.myinfo?.state = self.state
                                self.myinfo?.city = self.city
                                self.myinfo?.postal_code = self.postal_code
                                self.myinfo?.route = self.route
                                self.myinfo?.lat = self.lat
                                self.myinfo?.lon = self.lon
                                self.networkingService.updateUserstringinfo(user: self.myinfo, general : true)
                                
                            }
                            
                        }
                        
                    }catch {
                        print("Error")
                    }
                })
                
                // 5
                task.resume()
            }

            
            
            
            
            //Finally stop updating location otherwise it will come again and again in this delegate
          
        }
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
          FirebaseApp.configure()
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()


        
        
        
        
        
        
         GIDSignIn.sharedInstance().clientID=FirebaseApp.app()?.options.clientID
           GIDSignIn.sharedInstance().delegate = self
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
       Twitter.sharedInstance().start(withConsumerKey:"wDcF5H8yQ6xCg3o7uAzEEWpQP", consumerSecret:"Vd0kElRHHVAkVt1cyYlsGFJM5cB6hDfdddEbEUzX6k0el1V6E6")
        UIApplication.shared.statusBarStyle = .lightContent
        rendercurrentuser()
        self.takeToHome()
        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor guser: GIDGoogleUser!, withError error: Error!) {
        if let error = error
        {
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print("Failed to log into Google: " ,error)
            return
        }
        print("Successfully Logged into Google", guser)
        print(guser.profile.email)
        print(guser.profile.name)
        print(guser.profile.imageURL(withDimension: 120))
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Signing in Google ! ")
        guard let idtoken = guser.authentication.idToken else { return }
        guard let accessToken = guser.authentication.accessToken else { return }
        let credentials = GoogleAuthProvider.credential(withIDToken: idtoken, accessToken: accessToken)
        
        Auth.auth().signIn(with: credentials) { (user, error) in
            if error != nil {
            print("Failed to create a fire base user with google", error ?? "")
                return
            }
            
            guard (user?.uid) != nil else { return }
            print("sucessfully looged into firebase with google", user?.uid ?? "")
            
            
            let changeRequest = user?.createProfileChangeRequest()
            changeRequest?.displayName = guser.profile.name
            changeRequest?.photoURL = guser.profile.imageURL(withDimension: 120)
            
            changeRequest?.commitChanges(completion: { (error) in
                if error == nil {
                    
                    let userRef = self.databaseRef.child("users").child((user?.uid)!)
                    let newUser = Useri(email: guser.profile.email, fullname: guser.profile.name, uid: (user?.uid)!, profilePictureUrl: (user?.photoURL!.absoluteString)!)
                    
                    
                    userRef.observeSingleEvent(of: .value, with: { (currentUser) in
                        
                        
                        if(currentUser.exists())
                        {
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                            let appDel = UIApplication.shared.delegate as! AppDelegate
                            appDel.takeToHome()
                        }
                        else
                        {
                            
                            userRef.setValue(newUser.toAnyObject()) { (error, ref) in
                                if error == nil {
                                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                    self.takeToHome()
                                }else {
                                    print(error!.localizedDescription)
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                                    NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                }
                                
                                
                            }
                            
                        }
                        
                        
                        
                    }) { (error) in
                        let alertView = SCLAlertView()
                        alertView.showError("OOPS", subTitle: (error.localizedDescription))
                        print(error.localizedDescription)
                        
                    }
                    
                    
                    
                    
                   
                    
                    
                }else {
                    print(error!.localizedDescription)
                    let alertView = SCLAlertView()
                    alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                    NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                }
            })

            
            
            
            
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        /*let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        GIDSignIn.sharedInstance().handle(url as URL!,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,annotation: options[UIApplicationOpenURLOptionsKey.annotation])*/
        let directedByFB = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, options: options)
        let directedByTWTR =  Twitter.sharedInstance().application(application, open: url, options: options)
        let directedByGGL =  GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        return directedByGGL || directedByTWTR || directedByFB
    }
    func takeToHome(){
        if Auth.auth().currentUser != nil {
            Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
                if(token == nil)
                {
                    self.networkingService.logOut {}
                }
                else
                {
                    
                    self.networkingService.postToken(Token: Messaging.messaging().fcmToken as AnyObject)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "MainTabController") as! MainTabController
                    self.window?.rootViewController = homeVC
                }
            })
           
       }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        networkingService.postToken(Token: Messaging.messaging().fcmToken as AnyObject)
        
    }
    

    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    

}

