
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Event {
    
    
    var eventId: String?
    var sparkId: String?
    var userId: String?
    var date: NSNumber?
    var time: String?
    var description: String?
    var location: String?
    var postDate: NSNumber?
    var ref: DatabaseReference?
    var key: String = ""
    
    
    
    
    init(snapshot: DataSnapshot){
        
        
        
        self.eventId = (snapshot.value as! NSDictionary)["eventId"] as? String
        self.sparkId = (snapshot.value as! NSDictionary)["sparkId"] as? String
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.date = (snapshot.value as! NSDictionary)["date"] as? NSNumber
        self.time = (snapshot.value as! NSDictionary)["time"] as? String ?? ""
        self.description = (snapshot.value as! NSDictionary)["description"] as? String
        self.location = (snapshot.value as! NSDictionary)["location"] as? String
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    init(eventId: String, sparkId: String, userId: String, date: NSNumber,time: String, description: String, location: String, postDate: NSNumber ){
        
        
        
        self.eventId = eventId
        self.sparkId = sparkId
        self.userId = userId
        self.date = date
        self.time = time
        self.description = description
        self.location = location
        self.postDate = postDate
        self.ref = Database.database().reference()
        
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["eventId"] != nil)
        {
            self.eventId = initdic["eventId"]! as? String
        }
        if(initdic["sparkId"] != nil)
        {
            self.sparkId = initdic["sparkId"]! as? String
        }
        if(initdic["userId"] != nil)
        {
            self.userId = initdic["userId"]! as? String
        }
        if(initdic["date"] != nil)
        {
            self.date = initdic["date"]! as? NSNumber
        }
        if(initdic["time"] != nil)
        {
            self.time = initdic["time"]! as? String
        }
        if(initdic["description"] != nil)
        {
            self.description = initdic["description"]! as? String
        }
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
    }
    
    
    
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.eventId != nil)
        {
            initdic["eventId"] = self.eventId!
        }
        if(self.sparkId != nil)
        {
            initdic["sparkId"] = self.sparkId!
        }
        if(self.userId != nil)
        {
            initdic["userId"] = self.userId!
        }
        if(self.date != nil)
        {
            initdic["date"] = self.date!
        }
        if(self.time != nil)
        {
            initdic["time"] = self.time!
        }
        if(self.description != nil)
        {
            initdic["description"] = self.description!
        }
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        
        
        return initdic
    }
    
    
    
    
    
}


