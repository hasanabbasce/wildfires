//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Useri {
    
    var email: String?
    var fullname: String?
    var uid: String?
    var profilePictureUrl: String?
    var ref: DatabaseReference?
    var key: String = ""
    var isVerified: Bool = false
    
    
    var location: String?
    var lat: Double?
    var lon: Double?
    var country: String?
    var state: String?
    var city: String?
    var postal_code: String?
    var route: String?
    var postDate: NSNumber?
    var about: String?
    
    var action: String?
    
    
    init?(snapshot: DataSnapshot){
        if snapshot.value is NSNull {
            
            if Auth.auth().currentUser != nil {
                
                do {
                    
                    try Auth.auth().signOut()
                }
                    
                catch let error {
                    
                    print("Failed to log out user: \(error.localizedDescription)")
                }
            }
            return nil
        }
        self.email = (snapshot.value as! NSDictionary)["email"] as? String ?? ""
        self.fullname = (snapshot.value as! NSDictionary)["fullname"] as? String ?? ""
        self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.profilePictureUrl = (snapshot.value as! NSDictionary)["profilePictureUrl"] as? String ?? ""
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.isVerified = (snapshot.value as! NSDictionary)["isVerified"] as? Bool ?? false
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.lon = (snapshot.value as! NSDictionary)["lon"] as? Double ?? 0
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.state = (snapshot.value as! NSDictionary)["state"] as? String ?? ""
        self.city = (snapshot.value as! NSDictionary)["city"] as? String ?? ""
        self.postal_code = (snapshot.value as! NSDictionary)["postal_code"] as? String ?? ""
        self.route = (snapshot.value as! NSDictionary)["route"] as? String ?? ""
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
        if(self.uid == Auth.auth().currentUser?.uid)
        {
            
            if let blockeduser = ((snapshot.value as! NSDictionary)["blockeduser"])
            {
                var blocked_array = [String]()
                for (key, element) in blockeduser  as! NSDictionary {
                    if(!(key is NSNull))
                    {
                        blocked_array.append(key as! String)
                    }
                }
                UserDefaults.standard.set(blocked_array, forKey: "blockusers")
                
            }
            else
            {
                UserDefaults.standard.set([], forKey: "blockusers")
            }
        }
        
        
    }
    
     init(email: String, fullname: String, uid: String, profilePictureUrl: String ){
        
        
        
        self.email = email
        self.fullname = fullname
        self.uid = uid
        self.profilePictureUrl = profilePictureUrl
        self.ref = Database.database().reference()
        self.postDate = NSDate().timeIntervalSince1970 as NSNumber
        
    }
    
     init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["email"] != nil)
        {
         self.email = initdic["email"]! as? String
        }
        if(initdic["fullname"] != nil)
        {
            self.fullname = initdic["fullname"]! as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]!  as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["profilePictureUrl"] != nil)
        {
            self.profilePictureUrl = initdic["profilePictureUrl"]! as? String
        }
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lon = initdic["lon"]! as? Double
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["state"] != nil)
        {
            self.state = initdic["state"]! as? String
        }
        if(initdic["city"] != nil)
        {
            self.city = initdic["city"]! as? String
        }
        if(initdic["postal_code"] != nil)
        {
            self.postal_code = initdic["postal_code"]! as? String
        }
        if(initdic["route"] != nil)
        {
            self.route = initdic["route"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
       
        
    }
    
    func getFullname() -> String{
        
        return "\(fullname) "
    }
    
    
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.email != nil)
        {
            initdic["email"] = self.email!
        }
        if(self.fullname != nil)
        {
            initdic["fullname"] = self.fullname!
            initdic["fullname_lowercase"] = self.fullname!.lowercased()
            
        }
        if(self.about != nil)
        {
            initdic["about"] = self.about!
        }
        
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.profilePictureUrl != nil)
        {
            initdic["profilePictureUrl"] = self.profilePictureUrl!
        }
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lon != nil)
        {
            initdic["lon"] = self.lon!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.state != nil)
        {
            initdic["state"] = self.state!
        }
        
        if(self.city != nil)
        {
            initdic["city"] = self.city!
        }
        if(self.postal_code != nil)
        {
            initdic["postal_code"] = String(self.postal_code!)
        }
        if(self.route != nil)
        {
            initdic["route"] = self.route!
        }
        if(self.country != nil)
        {
            initdic["country"] = String(self.country!)
        }
        
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        
        return initdic
    }
    
    
    
    
    
    
}
