//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView

class UpdateprofileViewController : UIViewController , UITextFieldDelegate , UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate , UITextViewDelegate , PlaceSearchResultsControllerDelegate {
    
    @IBOutlet weak var fullnamefield: SkyFloatingLabelTextField!
   
    @IBOutlet weak var Location: SkyFloatingLabelTextField!
    
    @IBOutlet weak var profileImageView: circularimage!
    
     @IBOutlet weak var abouttextview: UITextView!
    @IBOutlet weak var updatebtn: UIButton!
    
    var networkingService = NetworkingService()
    
    let activityData = ActivityData.init(size: nil, message: "Creating Profile", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
   // var userlocation: String = ""
   // var country: String = ""
   // var state: String = ""
  //  var city: String = ""
  //  var postal_code: String = ""
  //  var route: String = ""
  //  var log: Double = 0
  //  var lat: Double = 0
    var profileimageupdate: Bool = false
  //  var locationupdate: Bool = false
    var meuser:Useri?
    
    var PlaceSearchResultsControllervar:PlaceSearchResultsController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //updatebtn.isEnabled = false
        print("sss")
        Location.delegate = self
        abouttextview.delegate = self
        abouttextview!.layer.borderWidth = 1
        abouttextview!.layer.borderColor = UIColor.lightGray.cgColor
        PlaceSearchResultsControllervar = PlaceSearchResultsController()
        PlaceSearchResultsControllervar.delegate = self
        
        setTapGestureRecognizerOnView()
        setSwipeGestureRecognizerOnView()
        assignbackground()
        addfields()
        renderuser()
    }
    
    private func renderuser()
    {
        //networkingService.fetchCurrentUser { (user) in
          //  if let user = user {
          //      self.meuser = user
                self.profileImageView.sd_setImage(with: URL(string: (self.meuser?.profilePictureUrl!)!), placeholderImage: UIImage(named: "default"))
                print("here")
                self.fullnamefield.text = self.meuser?.fullname
                self.Location.text = self.meuser?.location
                self.abouttextview.text = self.meuser?.about
                self.updatebtn.isEnabled = true
           // }
       // }
    }
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String,location_details : [ String : String ]) {
       // locationupdate = true
        Location.text = title
       // self.userlocation = title
        self.meuser?.location = title
        if let country = location_details["country"] {
           // self.country = country
            self.meuser?.country = country
        }
        if let state = location_details["state"] {
            //self.state = state
             self.meuser?.state = state
        }
        if let city = location_details["city"] {
            //self.city = city
             self.meuser?.city = city
        }
        if let postal_code = location_details["postal_code"] {
            //self.postal_code = postal_code
            self.meuser?.postal_code = postal_code as? String
        }
        if let route = location_details["route"] {
          //  self.route = route
            self.meuser?.route = route as? String
        }
        
        self.meuser?.lon = lon
        self.meuser?.lat = lat
      //  self.log = lon
      //  self.lat = lat
        
        print(title)
    }
   
    
    func addfields()
    {
        
        if let fullnamefield = fullnamefield {
            fullnamefield.placeholder = "FULL NAME"
            fullnamefield.title = "FULL NAME"
            
            fullnamefield.delegate = self
        }
        
      
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
                if(floatingLabelTextField.placeholder == "EMAIL")
                {
                    if(text.characters.count < 3 || !text.contains("@")) {
                        floatingLabelTextField.errorMessage = "Invalid email"
                    }
                    else {
                        // The error message will only disappear when we reset it to nil or empty string
                        floatingLabelTextField.errorMessage = ""
                    }
                }
            }
        }
        return true
    }
    
    func assignbackground(){
        let background = UIImage(named: "bg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    @IBAction func chooseprofileimage(_ sender: Any) {
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = profileImageView
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
           // popoverController.sourceView = self.view
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
           // popoverController.permittedArrowDirections = []
            popoverController.sourceView = sender as? UIView
            popoverController.sourceRect = (sender as AnyObject).bounds;
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.left
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func updateAccount(_ sender: Any) {
       print("here")
        let fullname = fullnamefield?.text!
        let abouttext = abouttextview?.text!
        var data : Data?
        meuser?.fullname = fullname
        meuser?.about = abouttext
        if(profileimageupdate)
        {
         data = UIImageJPEGRepresentation(profileImageView.image!, 0.2)!
        }
        
        
        if (fullname?.isEmpty)! || (Location.text?.isEmpty)! || (abouttext?.isEmpty)!  {
            
        }else {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Updating User ..  ")
            self.networkingService.updateUser(user: meuser, pictureData: data)
           
        }
        
        self.view.endEditing(true)
 
    }
    
    
    
    
}


extension UpdateprofileViewController {
    
    
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileimageupdate = true
            self.profileImageView.image = chosenImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        animateView(up: true, moveValue: 150)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateView(up: false, moveValue:
            150)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fullnamefield?.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if( textField != Location)
        {
            
            animateView(up: true, moveValue: 80)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == Location)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(PlaceSearchResultsControllervar, animated: false)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField != Location)
        {
            animateView(up: false, moveValue:
                80)
        }
    }
    
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    func setSwipeGestureRecognizerOnView(){
        let swipDown = UISwipeGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        swipDown.direction = .down
        self.view.addGestureRecognizer(swipDown)
    }
}

