//
//  ChatRoom.swift
//  WhatsAppClone
//
//  Created by Frezy Stone Mboumba on 8/27/16.
//  Copyright © 2016 Frezy Stone Mboumba. All rights reserved.
//

import Foundation
import FirebaseDatabase


struct ChatRoom {
    
    var username: String!
    var other_Username: String!
    var userId: String!
    var other_UserId: String!
    var members: [String]!
    var chatRoomId: String!
    var key: String = ""
    var lastMessage: String!
    var ref: DatabaseReference!
    var userPhotoUrl: String!
    var other_UserPhotoUrl: String!
    var date: NSNumber!
    
    init(snapshot: DataSnapshot){
        
        self.username = (snapshot.value as! NSDictionary)["username"] as? String
        self.other_Username = (snapshot.value as! NSDictionary)["other_Username"] as? String
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.other_UserId = (snapshot.value as! NSDictionary)["other_UserId"] as? String
        self.lastMessage = (snapshot.value as! NSDictionary)["lastMessage"] as? String
        self.userPhotoUrl = (snapshot.value as! NSDictionary)["userPhotoUrl"] as? String
        self.other_UserPhotoUrl = (snapshot.value as! NSDictionary)["other_UserPhotoUrl"] as? String
        self.members = (snapshot.value as! NSDictionary)["members"] as? [String]
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.chatRoomId = (snapshot.value as! NSDictionary)["chatRoomId"] as? String
        self.date = (snapshot.value as! NSDictionary)["date"] as? NSNumber
    }
    
    
    init(username: String, other_Username: String,userId: String,other_UserId: String,members: [String],chatRoomId: String,lastMessage: String,key: String = "",userPhotoUrl: String,other_UserPhotoUrl: String, date: NSNumber){
        
        self.username = username
        self.other_UserPhotoUrl = other_UserPhotoUrl
        self.other_Username = other_Username
        self.userId = userId
        self.other_UserId = other_UserId
        self.userPhotoUrl = userPhotoUrl
        self.members = members
        self.lastMessage = lastMessage
        self.chatRoomId = chatRoomId
        self.date = date
        
        
    }
    
    func toAnyObject() -> [String: AnyObject] {
        
        return ["username": username as AnyObject, "other_Username": other_Username as AnyObject,"userId": userId as AnyObject,"other_UserId": other_UserId as AnyObject,"members": members as AnyObject,"chatRoomId": chatRoomId as AnyObject,"lastMessage": lastMessage as AnyObject,"userPhotoUrl": userPhotoUrl as AnyObject,"other_UserPhotoUrl": other_UserPhotoUrl as AnyObject,"date":date]
        
    }
    
}
