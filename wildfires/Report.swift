
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Report {
    
    var problem : String?
    var reportItemId : String?
    var userId : String?
    var reportItem : String?
    
    var postDate: NSNumber?
    var ref: DatabaseReference?
    var key: String = ""
    
    
    
    
    init(snapshot: DataSnapshot){
        self.problem = (snapshot.value as! NSDictionary)["problem"] as? String
        self.reportItemId = (snapshot.value as! NSDictionary)["reportItemId"] as? String
        self.userId = (snapshot.value as! NSDictionary)["userId"] as? String
        self.reportItem = (snapshot.value as! NSDictionary)["reportItem"] as? String
        
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    
    init( reportItemId: String, userId: String, reportItem: String ,problem: String, postDate: NSNumber ){
        
        self.reportItemId = reportItemId
        self.reportItem = reportItem
        self.userId = userId
        self.problem = problem
        self.postDate = postDate
        self.ref = Database.database().reference()
        
    }
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["reportItemId"] != nil)
        {
            self.reportItemId = initdic["reportItemId"]! as? String
        }
        if(initdic["userId"] != nil)
        {
            self.userId = initdic["userId"]!  as? String
        }
        if(initdic["reportItem"] != nil)
        {
            self.reportItem = initdic["reportItem"]! as? String
        }
        if(initdic["problem"] != nil)
        {
            self.problem = initdic["problem"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
   
    
    
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.reportItemId != nil)
        {
            initdic["reportItemId"] = self.reportItemId!
        }
        if(self.userId != nil)
        {
            initdic["userId"] = self.userId!
        }
        
        if(self.reportItem != nil)
        {
            initdic["reportItem"] = self.reportItem!
        }
        if(self.problem != nil)
        {
            initdic["problem"] = self.problem!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        
        
        
        return initdic
    }
    
    
    
    
    
    
}

