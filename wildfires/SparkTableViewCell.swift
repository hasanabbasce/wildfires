//
//  SparkTableViewCell.swift
//  wildfires
//
//  Created by Hasan Abbas on 13/08/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import Firebase

protocol SparkTableViewCellDelegate{
    func didtapviewbutton(Spark: Spark,Cell : UITableViewCell)
    func didtapjoinbutton(Spark: Spark,Cell : SparkTableViewCell, custom : Any?, sender : Any?)
    func didtapprofilebutton(user: Useri)
}
class SparkTableViewCell: UITableViewCell {
    
    var delegate: SparkTableViewCellDelegate!
    
    @IBOutlet weak var coverimage: UIImageView!
    
    @IBOutlet weak var sparkicon: UIImageView!
    @IBOutlet weak var sparktitle: UILabel!
    @IBOutlet weak var profileimage: CustomizableImageView!
    @IBOutlet weak var sparkdescription: UILabel!
    
   // @IBOutlet weak var viewspark: UIButton!
    @IBOutlet weak var joinspark: UIButton!
    @IBOutlet weak var heart: UIButton!
    @IBOutlet weak var share: UIButton!
    @IBOutlet weak var commentbtn: UIButton!
    
    @IBOutlet weak var fullname: UILabel!
    @IBOutlet weak var postdate: UILabel!
    
    @IBOutlet weak var countdetails: UILabel!
    
    var sparkjoin : Bool = false
    var sparkheart : Bool = false
  //  var sparkid : String?
    var spark : Spark?
    
    var networkingService = NetworkingService()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
 
   
    }
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.sparkdescription.sizeToFit()
        self.contentView.layoutIfNeeded()
        self.sparkdescription.preferredMaxLayoutWidth = self.sparkdescription.frame.size.width
        
        
    }
    
    func configureCell(Spark:  Spark){
     //   sparkid = Spark.sparkId
     
        commentbtn.tintColor = UIColor.lightGray
        share.tintColor = UIColor.lightGray
        spark = Spark
        self.joinspark.tintColor = UIColor.lightGray
        self.heart.tintColor = UIColor.lightGray
        self.joinspark.isHidden = true
        self.heart.isHidden = true
        self.coverimage?.sd_setImage(with: URL(string: Spark.cover!))
        let sparkdescription = Spark.description?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.sparkdescription?.text = sparkdescription
        self.sparktitle?.text = Spark.title
        self.profileimage?.sd_setImage(with: URL(string: Spark.profileptictureUrl!))
        self.sparkdescription?.sizeToFit()
        
        self.fullname?.text = Spark.SparkUser?.fullname
        self.sparkdescription?.updateConstraintsIfNeeded()
        self.sparkdescription?.resolveHashTags()
       /* if(Spark.userId == Auth.auth().currentUser?.uid )
        {
        self.joinspark.setImage(UIImage(named: "editspark-active.png"), for: .normal)
            self.joinspark.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
        }
        else
        {*/
            sparkjoin = (spark?.joinstatus)!
            sparkheart = (spark?.heartstatus)!
            //self.joinspark.setImage(UIImage(named: "fire-active.png"), for: .normal)
          if(spark?.joinstatus)!
          {
            self.joinspark.setTitle(" Leave",for: .normal)
            self.joinspark.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
            
          }
          else
          {
            self.joinspark.setTitle(" Join",for: .normal)
            self.joinspark.tintColor = UIColor.lightGray
          }
            
            if(spark?.heartstatus)!
            {
                self.heart.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
               
            }
            else
            {
                 self.heart.tintColor = UIColor.lightGray
            }
            
       // }
        self.joinspark.isHidden = false
        self.heart.isHidden = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.profiletap))
        
        tapGesture.numberOfTapsRequired = 1
        self.profileimage.addGestureRecognizer(tapGesture)
        
        
        let tapGesturename = UITapGestureRecognizer(target: self, action: #selector(self.profiletap))
        
        tapGesturename.numberOfTapsRequired = 1
        self.fullname.addGestureRecognizer(tapGesturename)
        
        let tapGesturespark = UITapGestureRecognizer(target: self, action: #selector(self.sparktap))
        
        tapGesturespark.numberOfTapsRequired = 1
        self.coverimage.addGestureRecognizer(tapGesturespark)
        
        let tapGesturespark2 = UITapGestureRecognizer(target: self, action: #selector(self.sparktap))
        
        tapGesturespark2.numberOfTapsRequired = 1
         self.sparktitle.addGestureRecognizer(tapGesturespark2)
        
        
        
        let fromDate = NSDate(timeIntervalSince1970: TimeInterval(Spark.postDate!))
        let toDate = NSDate()
        
        
        
        postdate.text = timeAgoSinceDate(fromDate as Date, currentDate: toDate as Date, numericDates: true)
        
        var countdetailstext = ""
        var  comma = ""
        if(spark?.joincount != 0)
        {
            if let count = spark?.joincount {
                countdetailstext =  "\(count)"  + " Joins"
                comma = ","
            }
            
        }
        
        if(spark?.heartcount != 0)
        {
             if let count = spark?.heartcount {
            countdetailstext =  countdetailstext + comma +  " " + "\(count)" + " Likes"
                comma = ","
            }
        }
        
        if(spark?.commentcount != 0)
        {
             if let count = spark?.commentcount {
            countdetailstext =  countdetailstext + comma + " " + "\(count)" + " Comments"
            }
        }
        
        countdetails.text = countdetailstext
    }
    
    func sparktap(_ sender:AnyObject){
     self.delegate.didtapviewbutton(Spark: self.spark!, Cell: self)
        
    }
    
    func profiletap(_ sender:AnyObject){
        
        self.delegate.didtapprofilebutton(user: (self.spark?.SparkUser)!)
        
    }
 
//    @IBAction func ViewSpark(_ sender: Any) {
//     self.delegate.didtapviewbutton(Spark: self.spark!, Cell: self)
//     
//    }
   
    @IBAction func Joinuneditspark(_ sender: Any) {
        
        
       /* if(spark?.userId == Auth.auth().currentUser?.uid )
        {
            self.delegate.didtapjoinbutton(Spark: self.spark!, Cell: self, custom : nil)
        
        }
        else
        {*/
            if(sparkjoin != nil)
            {
                //self.joinspark.setImage(UIImage(named: "fire-active.png"), for: .normal)
                if(sparkjoin)
                {
                    self.joinspark.setTitle(" Join",for: .normal)
                    self.joinspark.tintColor = UIColor.lightGray
                }
                else
                {
                    self.joinspark.setTitle(" Leave",for: .normal)
                    self.joinspark.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
                }
                sparkjoin = !sparkjoin
                self.spark?.joinstatus = sparkjoin
                let joinspark = JoinedSpark(joinedsparkId: spark!.sparkId! + "-" + (Auth.auth().currentUser?.uid)! , userId: (Auth.auth().currentUser?.uid)!, sparkuserId: (spark?.userId)!, sparkId: (spark?.sparkId!)! , status : sparkjoin, heartstatus : sparkheart)
                networkingService.joinSpark(joinedspark: joinspark,type: "join", completed: {
                    //print("ff")
                    
                    
                })
             
            }
       // }
    }
    
    @IBAction func likespark(_ sender: Any) {
        if(sparkheart != nil)
        {
            
            if(sparkheart)
            {
                self.heart.tintColor = UIColor.lightGray
            }
            else
            {
                self.heart.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
            }
            sparkheart = !sparkheart
            self.spark?.heartstatus = sparkheart
            let joinspark = JoinedSpark(joinedsparkId: spark!.sparkId! + "-" + (Auth.auth().currentUser?.uid)! , userId: (Auth.auth().currentUser?.uid)!, sparkuserId: (spark?.userId)!, sparkId: (spark?.sparkId!)! , status : sparkjoin, heartstatus : sparkheart)
            networkingService.joinSpark(joinedspark: joinspark,type:"heart", completed: {
                //print("ff")
                
                
            })
        }
    }
    
    @IBAction func sharespark(_ sender: Any) {
        self.delegate.didtapjoinbutton(Spark: self.spark!, Cell: self, custom : "share",sender:sender)
    }
    
    @IBAction func sparkoptions(_ sender: Any) {
        self.delegate.didtapjoinbutton(Spark: self.spark!, Cell: self, custom : "option",sender:sender)
    }
    
    @IBAction func comments(_ sender: Any) {
        self.delegate.didtapjoinbutton(Spark: self.spark!, Cell: self, custom : "comments",sender:sender)
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     // self.updateConstraintsIfNeeded()
        // Configure the view for the selected state
    }
    
}
