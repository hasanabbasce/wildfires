//
//  NetworkingService.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase
import NVActivityIndicatorView
import FBSDKLoginKit
import SCLAlertView
import GeoFire
import FirebaseMessaging


struct NetworkingService {
    
      let activityData = ActivityData.init(size: nil, message: "Creating Profile", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    
    var databaseRef: DatabaseReference! {
        
        return Database.database().reference()
    }
    
    var storageRef: StorageReference! {
        
        return Storage.storage().reference()
    }
    
    
    
    
    func signUp(fullname:String, email: String,pictureData: Data,password:String){
      
       
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if let error = error {
                print(error.localizedDescription)
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: error.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
           }else {
                
               self.setUserInfo(user: user, fullname: fullname, pictureData: pictureData,password: password)
   
           }
        })
        
    }
    
    func facebookSignup()
    {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Signing in Facebook ! ")
        let accessToken=FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else {
            return
        }
        let credentials=FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        Auth.auth().signIn(with: credentials) { (user, error) in
            if(error != nil)
            {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print("somthing went wrong with facebook user : ",error ?? "")
                return
            }
            print("succusfully login with user :",user?.uid ?? "")
            self.getFBUserData(user: user)
           
            
        }
    }
    func getFBUserData(user: User!){
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if(error != nil)
                {
                    let alertView = SCLAlertView()
                    alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                    NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    return
                }
                
                if (error == nil){
                    let  resultdic = result as! [String : AnyObject]
                    print("here1")
                    print(result!)
                    let changeRequest = user.createProfileChangeRequest()
                    changeRequest.displayName = resultdic["name"] as? String
                    
                    if let imageURL = ((resultdic["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? URL
                    {
                    changeRequest.photoURL = imageURL
                    }
                    changeRequest.commitChanges(completion: { (error) in
                        if error == nil {
                            
                            let userRef = self.databaseRef.child("users").child(user.uid)
                            let newUser = Useri(email: (resultdic["email"] as! String) , fullname: (resultdic["name"] as! String), uid: user.uid, profilePictureUrl: String(describing: user.photoURL!))
                            
                            userRef.observeSingleEvent(of: .value, with: { (currentUser) in
                                
                               
                                if(currentUser.exists())
                                {
                                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                    let appDel = UIApplication.shared.delegate as! AppDelegate
                                    appDel.takeToHome()
                                }
                                else
                                {
                                    userRef.setValue(newUser.toAnyObject()) { (error, ref) in
                                        if error == nil {
                                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                            let appDel = UIApplication.shared.delegate as! AppDelegate
                                            appDel.takeToHome()
                                        }else {
                                            let alertView = SCLAlertView()
                                            alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                                            NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                        }
                                        
                                        
                                    }
                                }
                                
                                
                                
                            }) { (error) in
                                let alertView = SCLAlertView()
                                alertView.showError("OOPS", subTitle: (error.localizedDescription))
                                print(error.localizedDescription)
                                
                            }
                            
                            
                           
                            
                            
                        }else {
                            print(error!.localizedDescription)
                            let alertView = SCLAlertView()
                            alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                            NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                            
                        }
                    })
                    
                
                }
            })
        }
    }
    private func setUserInfo(user: User!, fullname: String,pictureData: Data,password: String){
        
        let profilePicturePath = "profileImage/\(user.uid)image.jpg"
        let profilePictureRef = storageRef.child(profilePicturePath)
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        profilePictureRef.putData(pictureData, metadata: metaData) { (newMetadata, error) in
            if let error = error {
                print(error.localizedDescription)
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error.localizedDescription))
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }else {
                
                let changeRequest = user.createProfileChangeRequest()
                changeRequest.displayName = "\(fullname) "
                
                if let url = newMetadata?.downloadURL() {
                    changeRequest.photoURL = url
                }
                
                changeRequest.commitChanges(completion: { (error) in
                    if error == nil {
                        let userRef = self.databaseRef.child("users").child(user.uid)
                   let newUser = Useri(email: user.email!, fullname: fullname, uid: user.uid, profilePictureUrl: String(describing: user.photoURL!))
                        userRef.setValue(newUser.toAnyObject()) { (error, ref) in
                            if error == nil {
                                NVActivityIndicatorPresenter.sharedInstance.setMessage("Signed up Successfully ! ")
                                 self.signIn(email: user.email!, password: password)
                              //  self.saveUserInfoToDb(user: user, fullname: fullname ,password: password)
                            }else {
                                print(error!.localizedDescription)
                                let alertView = SCLAlertView()
                                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                                NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                            }
                            
                            
                        }
                        
                        
                    }else {
                        print(error!.localizedDescription)
                        let alertView = SCLAlertView()
                        alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                        NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        
                    }
                })
                
            }
        }
        
        
        
    }
    
    
    func updateUser(user:  Useri!,pictureData: Data?)
    {
        
        if  pictureData != nil {
        
        
        let profilePicturePath = "profileImage/\(String(describing: user.uid))image.jpg"
        let profilePictureRef = storageRef.child(profilePicturePath)
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        profilePictureRef.putData(pictureData!, metadata: metaData) { (newMetadata, error) in
            if let error = error {
                print(error.localizedDescription)
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error.localizedDescription))
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }else {
                if let url = newMetadata?.downloadURL() {
                    var changeuser = user
                    changeuser?.profilePictureUrl = url.absoluteString
                    self.updateUserstringinfo(user: changeuser)
                }
                
                        
                        
                
                
            }
        }
            
        }
        else
        {
          self.updateUserstringinfo(user: user)
        
        }

    }
    
    public func updateUserstringinfo(user: Useri!,general : Bool = false)
    {
       print("updating")
        let userRef = self.databaseRef.child("users").child(user.uid!)
        
        userRef.updateChildValues(user.toAnyObject()) { (error, ref) in
            if error == nil {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if(appDelegate.myinfo == nil)
                {
                    appDelegate.rendercurrentuser()
                }
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Profile Update Successfully ! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                if(!general)
                {
                let alertView = SCLAlertView()
                alertView.showSuccess("Profile", subTitle: "Profile Update Successfully !")
                }
                //  self.saveUserInfoToDb(user: user, fullname: fullname ,password: password)
            }else {
                print(error!.localizedDescription)
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            
            
        }
    }
    
    
    
 
    
    
    
    func signIn(email: String, password: String){
        if(NVActivityIndicatorPresenter.sharedInstance.isHidden())
        {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            NVActivityIndicatorPresenter.sharedInstance.setMessage("logging in ")
        }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                if let user = user {
                    print("\(user.displayName!) has logged in successfully!")
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("\(user.displayName!) has logged in ")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.takeToHome()
                    
                    
                }
                
            }else {
                print(error!.localizedDescription)
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
            }
        })
        
    }
    
         
    
    func fetchPostUserInfo(uid: String, completion: @escaping (Useri?)->()){
        
        
        let userRef = databaseRef.child("users").child(uid)
        
        userRef.observeSingleEvent(of: .value, with: { (currentUser) in
            
            let user: Useri = Useri(snapshot: currentUser)!
            completion(user)
            
            
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
            
        }
        
        
    }
    
    func fetchCurrentUser(completion: @escaping (Useri?)->()){
       
        let currentUser = Auth.auth().currentUser!
        
        let currentUserRef =  databaseRef.child("users").child(currentUser.uid)
        
        currentUserRef.observe(.value, with: { (currentUser) in
            if(currentUser.exists())
            {
            let  user: Useri = Useri(snapshot: currentUser)!
            completion(user)
            }
            else
            {
                self.logOut {
                    
                }
            completion(nil)
            }
            
        }) { (error) in
            completion(nil)
            //let alertView = SCLAlertView()
           // alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
            
        }
       
        
    }
    
    func fetchAllUsers(completion: @escaping([Useri])->Void){
        
        let usersRef = databaseRef.child("users")
        usersRef.observe(.value, with: { (users) in
            
            var resultArray = [Useri]()
            for user in users.children {
                
                let user = Useri(snapshot: user as! DataSnapshot)
                let currentUser = Auth.auth().currentUser!
                
                if user?.uid != currentUser.uid {
                    resultArray.append(user!)
                }
                completion(resultArray)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
        
    }
    
    func SearchAllUsers(textstring : String , completion: @escaping([Useri])->Void){
        
        
        databaseRef.child("users").queryOrdered(byChild:  "fullname_lowercase").queryStarting(atValue: textstring.lowercased()).queryEnding(atValue: textstring.lowercased() + "\u{f8ff}").observeSingleEvent(of: .value, with: { (snapshot) in
            
           var resultArray = [Useri]()
            for usersnap in snapshot.children.allObjects as! [DataSnapshot] {
                print(usersnap)
                resultArray.append(Useri(snapshot: usersnap)!)
               
            }
            completion(resultArray)
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func fetchGuestUser(ref:DatabaseReference!, completion: @escaping (Useri?)->()){
        
        
        ref.observeSingleEvent(of: .value, with: { (currentUser) in
            
            let user: Useri = Useri(snapshot: currentUser)!
            completion(user)
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
            
        }
        
        
        
    }
    
  
    
    
    
    
    
   
    
   
    
    func downloadImageFromFirebase(urlString: String, completion: @escaping (UIImage?)->()){
        
        let storageRef = Storage.storage().reference(forURL: urlString)
        
        storageRef.getData(maxSize: 1 * 1024 * 1024) { (imageData, error) in
            if error != nil {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                print(error!.localizedDescription)
            } else {
                
                if let data = imageData {
                    completion(UIImage(data:data))
                    
                }
            }
        }
        
        
    }
    
   
    
     func addsparkToDB(spark: Spark, completed: @escaping ()->Void){
       
       let sparkRef = databaseRef.child("spark").child(spark.sparkId!)
      sparkRef.setValue(spark.toAnyObject()) { (error, ref) in
        if let error = error {
            completed()
                print(error.localizedDescription)
           }else {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Spark Added Successfully ! ")
            
            self.savelocationspark(Sparkid: spark.sparkId!, sparkLocation :  CLLocation.init(latitude: spark.lat!, longitude: spark.lon!) )
            completed()
            }
        }
     }
    
    func uploadsparkImageToFirebase(sparkId: String,imageData: Data, completion: @escaping (URL)->()){
        
        let sparkImagePath = "sparkimages/\(sparkId)image.jpg"
        let sparkImageRef = storageRef.child(sparkImagePath)
        let sparkImageMetadata = StorageMetadata()
        sparkImageMetadata.contentType = "image/jpeg"
        
        
        sparkImageRef.putData(imageData, metadata: sparkImageMetadata) { (newsparkImageMD, error) in
            if let error = error {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error.localizedDescription))
                print(error.localizedDescription)
            }else {
                if let sparkImageURL = newsparkImageMD?.downloadURL() {
                    completion(sparkImageURL)
                }
            }
        }
        
    }
    
    
    
    
    
    func updateSpark(spark: Spark!, pictureData : Data?, completed: @escaping ()->Void)
    {
        let dgroup: DispatchGroup = DispatchGroup()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Updating Spark .. ")
        dgroup.enter()
        var finalspark = spark
        if  pictureData != nil {
            uploadsparkImageToFirebase(sparkId: (finalspark?.sparkId!)!, imageData: pictureData!, completion: { (coverurl) in
                finalspark?.cover = coverurl.absoluteString
                 dgroup.leave()
            })
        }
        else
        {
        dgroup.leave()
        }
        
        dgroup.notify(queue: DispatchQueue.main) {
            let sparkRef = self.databaseRef.child("spark").child((finalspark?.sparkId!)!)
            sparkRef.updateChildValues((finalspark?.toAnyObject())!) { (error, ref)  in
                if error == nil {
                    self.savelocationspark(Sparkid: spark.sparkId!, sparkLocation :  CLLocation.init(latitude: spark.lat!, longitude: spark.lon!) )
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Spark Update Successfully ! ")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let alertView = SCLAlertView()
                    alertView.showSuccess("Spark", subTitle: "Spark Update Successfully !")
                    completed()
                    //  self.saveUserInfoToDb(user: user, fullname: fullname ,password: password)
                }else {
                    print(error!.localizedDescription)
                    let alertView = SCLAlertView()
                    alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
                    NVActivityIndicatorPresenter.sharedInstance.setMessage(error?.localizedDescription)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                
            }
        }
        
       
    }
    
    func addtagToDB(tag: Tags, completed: @escaping ()->Void){
        
        let tagRef = databaseRef.child("tags").childByAutoId()
        tagRef.setValue(tag.toAnyObject()) { (error, ref) in
            if let error = error {
                completed()
                print(error.localizedDescription)
            }else {
               
                completed()
            }
        }
    }
    
    
    func fetchAllSparks(date : NSNumber , completion: @escaping ([Spark])->()){
        
        let sparkRef = databaseRef.child("spark")
        sparkRef.queryOrdered(byChild:  "postDate").queryStarting(atValue: date).observe(.value, with: { (sparks) in
            
            var resultArray = [Spark]()
            let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in sparks.children {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap as! DataSnapshot)
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                            spark.joinstatus = (joinedspark?.status)!
                            spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray)
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    func fetchAllSparkspaginate(date : NSNumber ,limit : Int , completion: @escaping([Spark] , _ date : NSNumber )->Void){
        
        
        databaseRef.child("spark").queryOrdered(byChild:  "postDate").queryEnding(atValue: date.intValue-1).queryLimited(toLast: UInt(limit)).observe( .value, with: { (snapshot) in
            print(snapshot.value)
            if !(snapshot.value is NSNull)
            {
            var resultArray = [Spark]()
            let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap)
             //   if(spark.postDate != date)
             //   {
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                            spark.joinstatus = (joinedspark?.status)!
                            spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                            
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                    
                    
                    
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }
                    
               // }
              //  else
             //   {
             //   dgroup.leave()
             //   }
                
                
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray, date)
            }
                
            }
            else
            {
            completion([], date)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
             print("sssgdddg")
        }
        
        
        
    }
    
    func fetchAllSparksofuser(userid : String , completion: @escaping([Spark])->Void){
        
        print("here i come" + userid)
        databaseRef.child("spark").queryOrdered(byChild:  "userId").queryEqual(toValue: userid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [Spark]()
            let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap)
                
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                            spark.joinstatus = (joinedspark?.status)!
                            spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                            
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }
                
                
                
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func SearchAllTagSparks(textstring : String , completion: @escaping([Spark])->Void){
        
        var replacedtextstring = textstring.replacingOccurrences(of: "#", with: "", options: .literal, range: nil)
        databaseRef.child("spark").queryOrdered(byChild:  "tag-"+replacedtextstring.lowercased()).queryEqual(toValue: true).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [Spark]()
            let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap)
                
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                            spark.joinstatus = (joinedspark?.status)!
                            spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                            
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }
                
                
                
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func SearchAllSparks(textstring : String , completion: @escaping([Spark])->Void){
        
        
        databaseRef.child("spark").queryOrdered(byChild:  "title_lowercase").queryStarting(atValue: textstring.lowercased()).queryEnding(atValue: textstring.lowercased() + "\u{f8ff}").observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [Spark]()
             let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap)
                
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                              spark.joinstatus = (joinedspark?.status)!
                              spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                            
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }

                
               
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
        
        
        
    }
    
    func fetchSparkbyid(sparkid: String, completion: @escaping (Spark?)->()){
        
        
        let sparkRef = databaseRef.child("spark").child(sparkid)
        
        sparkRef.observeSingleEvent(of: .value, with: { (sparks) in
            let dgroup: DispatchGroup = DispatchGroup()
            var spark: Spark = Spark(snapshot: sparks)
            dgroup.enter()
            
            let UserRef = self.databaseRef.child("users").child(spark.userId!)
            
            UserRef.observeSingleEvent(of: .value, with: { (User) in
                
                let user: Useri = Useri(snapshot: User)!
                
                spark.profileptictureUrl = user.profilePictureUrl
                spark.username = user.fullname
                
                self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                    if(joinedspark != nil)
                    {
                        spark.joinstatus = (joinedspark?.status)!
                        
                    }
                    else
                    {
                        spark.joinstatus = false
                        
                    }
                    dgroup.leave()
                    
                }
            }) { (error) in
                
                print(error.localizedDescription)
                
            }
            
            dgroup.notify(queue: DispatchQueue.main) {
            completion(spark)
            }
            
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
            
        }
        
        
    }
    
    
    func addcommentToDB(comment: Comments, completed: @escaping ()->Void){
        
        let commentRef = databaseRef.child("comment").childByAutoId()
        commentRef.setValue(comment.toAnyObject()) { (error, ref) in
            if let error = error {
                print(error.localizedDescription)
            }else {
                let dgroup: DispatchGroup = DispatchGroup()
                dgroup.enter()
                
                let sparkRef = self.databaseRef.child("spark").child(comment.sparkId!)
                sparkRef.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                    if var data = currentData.value as? [String: Any] {
                        
                        self.addnotification(object: Notifications(identifier: "commentspark", object: comment.toAnyObject()), completed: {
                            dgroup.leave()
                        })
                    
                            var count = data["commentcount"] as? Int ?? 0
                            
                        
                                count += 1
                            data["commentcount"] = count
                        
                        currentData.value = data
                    }
                    return TransactionResult.success(withValue: currentData)
                }
                
                
                
                
                dgroup.notify(queue: DispatchQueue.main) {
                    completed()
                }
            }
        }
    }
    
    
    func fetchcommentsfromDB(Sparkid: String, completion: @escaping([Comments])->Void){
        
        databaseRef.child("comment").queryOrdered(byChild:  "sparkId").queryEqual(toValue: Sparkid).observe(.value, with: { (snapshot) in
            let dgroup: DispatchGroup = DispatchGroup()
            var resultArray = [Comments]()
            for commentsnap in snapshot.children.allObjects as! [DataSnapshot] {
                var Comment: Comments = Comments(snapshot: commentsnap)
                dgroup.enter()
                let UserRef = self.databaseRef.child("users").child(Comment.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    Comment.profileimage = user.profilePictureUrl
                    Comment.username = user.fullname
                    
                    resultArray.append(Comment)
                    dgroup.leave()
                }) { (error) in
    
                    print(error.localizedDescription)
                    
                }
                
                
            }
             dgroup.notify(queue: DispatchQueue.main) {
             completion(resultArray)
             }
            
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }
    
    func fetcheventsfromDB(Sparkid: String, completion: @escaping([Event])->Void){
        
        databaseRef.child("events").queryOrdered(byChild:  "sparkId").queryEqual(toValue: Sparkid).observe(.value, with: { (snapshot) in
            let dgroup: DispatchGroup = DispatchGroup()
            var resultArray = [Event]()
            for commentsnap in snapshot.children.allObjects as! [DataSnapshot] {
                var Eventt: Event = Event(snapshot: commentsnap)
                resultArray.append(Eventt)
                
                
            }
             completion(resultArray)
            
            
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }
    
    func addevent(event: Event, completed: @escaping ()->Void){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Creating Event ! ")
        let eventRef = databaseRef.child("events").childByAutoId()
        eventRef.setValue(event.toAnyObject()) { (error, ref) in
            if let error = error {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Error Creating Event! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                completed()
                print(error.localizedDescription)
            }else {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Event Created Successfully ! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                let alertView = SCLAlertView()
                alertView.showSuccess("Event Created", subTitle: "Event Created Successfully !")
                completed()
            }
        }
    }
    
    
    
    
    
    func logOut(completion: ()->()){
        
        
        if Auth.auth().currentUser != nil {
            
            do {
                
                try Auth.auth().signOut()
                completion()
            }
                
            catch let error {
              //  let alertView = SCLAlertView()
               // alertView.showError("OOPS", subTitle: (error.localizedDescription))
                print("Failed to log out user: \(error.localizedDescription)")
            }
        }
        
    }
    
    func joinSpark(joinedspark: JoinedSpark, type : String , completed: @escaping ()->Void)
    {
        let joinedsparkRef = databaseRef.child("joinedspark").child(joinedspark.joinedsparkId!)
        joinedsparkRef.setValue(joinedspark.toAnyObject()) { (error, ref) in
            if let error = error {
                print(error.localizedDescription)
            }else {
                let dgroup: DispatchGroup = DispatchGroup()
                dgroup.enter()
                var notidentifier : String = ""
                
                let sparkRef = self.databaseRef.child("spark").child(joinedspark.sparkId!)
                sparkRef.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                    
                    if var data = currentData.value as? [String: Any] {
                        
                        if(type == "join")
                        {
                        var count = data["joincount"] as? Int ?? 0
                        
                        if(joinedspark.status)!
                        {
                            notidentifier = "joinspark"
                        count += 1
                        }
                        else
                        {
                        count -= 1
                        }
                        data["joincount"] = count
                        }
                        if(type == "heart")
                        {
                            var count = data["heartcount"] as? Int ?? 0
                            
                            if(joinedspark.heartstatus)!
                            {
                                notidentifier = "likespark"
                                count += 1
                            }
                            else
                            {
                                count -= 1
                            }
                            data["heartcount"] = count
                        }
                        
                        currentData.value = data
                    }
                    if(notidentifier != "")
                    {
                        self.addnotification(object: Notifications(identifier: notidentifier, object: joinedspark.toAnyObject()), completed: {
                            dgroup.leave()
                        })
                    }
                    else
                    {
                    dgroup.leave()
                    }
                    
                    
                    return TransactionResult.success(withValue: currentData)
                }
                
                
                
                
                dgroup.notify(queue: DispatchQueue.main) {
                completed()
                }
                
            }
        }
    }
    
    func fetchjoinedspark(joindsparkid: String, completion: @escaping (JoinedSpark?)->()){
        
        
        let joinedsparkRef = databaseRef.child("joinedspark").child(joindsparkid)
        
        joinedsparkRef.observeSingleEvent(of: .value, with: { (joinedspark) in
            
            if(joinedspark.exists())
            {
            let joinedsparkthis: JoinedSpark = JoinedSpark(snapshot: joinedspark)
            completion(joinedsparkthis)
            }
            else
            {
            completion(nil)
            }
            
            
            
            
        }) { (error) in
            
            print(error.localizedDescription)
            
        }
        
        
    }
    
    
    func savelocationspark(Sparkid : String, sparkLocation : CLLocation )
    {
    let sparkRef = databaseRef.child("sparklocation")
    let GeoFirevar = GeoFire(firebaseRef: sparkRef)
       // let currentLocation = CLLocation.init(latitude: 37.6549406, longitude: -5.528099099999999)
        GeoFirevar?.setLocation(sparkLocation, forKey: Sparkid) { (error) in
            
            if (error != nil) {
                
                print("An error occured: \(error)")
                
            } else {
                
                print("Saved location successfully!")
                
            }
            
        }
 

    }
    
    func savelocationuser(userid : String, userLocation : CLLocation )
    {
        let userRef = databaseRef.child("userlocation")
        let GeoFirevar = GeoFire(firebaseRef: userRef)
        // let currentLocation = CLLocation.init(latitude: 37.6549406, longitude: -5.528099099999999)
        GeoFirevar?.setLocation(userLocation, forKey: userid) { (error) in
            
            if (error != nil) {
                
                print("An error occured: \(error)")
                
            } else {
                
                print("Saved location successfully!")
                
            }
            
        }
        
        
    }
    
    func fetchSparknearby(currentLocation : CLLocation, radius : Int, completion: @escaping([Spark])->Void )
    {
        
        var sparkkeyarray = [String]()
        var sparksarray = [Spark]()
        let sparkRef = databaseRef.child("sparklocation")
        let GeoFirevar = GeoFire(firebaseRef: sparkRef)
        print(currentLocation.coordinate.longitude)
         print(currentLocation.coordinate.latitude)
        let query = GeoFirevar?.query(at: currentLocation, withRadius: Double(radius))
        
        
        
        let queryobserve = query?.observe(.keyEntered, with: {
            (key: String!, location: CLLocation!) in
            print(key)
            sparkkeyarray.append(key)
            
        })
        query?.observeReady({
            
            if(sparkkeyarray.count > 0)
            {
                let dgroup: DispatchGroup = DispatchGroup()
                
            for sparkid in sparkkeyarray {
                
                let sparkRef = self.databaseRef.child("spark").child(sparkid)
                dgroup.enter()
                sparkRef.observeSingleEvent(of: .value, with: { (sparks) in
                    
                    if(sparks.exists())
                    {
                    var spark: Spark = Spark(snapshot: sparks)
                    let UserRef = self.databaseRef.child("users").child(spark.userId!)
                    
                    UserRef.observeSingleEvent(of: .value, with: { (User) in
                        
                        let user: Useri = Useri(snapshot: User)!
                        
                        spark.profileptictureUrl = user.profilePictureUrl
                        spark.username = user.fullname
                        spark.SparkUser = user
                        self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                            if(joinedspark != nil)
                            {
                                spark.joinstatus = (joinedspark?.status)!
                                spark.heartstatus = (joinedspark?.heartstatus)!
                            }
                            else
                            {
                                spark.joinstatus = false
                                spark.heartstatus = false
                                
                            }
                            sparksarray.append(spark)
                            dgroup.leave()
                        }
                    }) { (error) in
                        print(error.localizedDescription)
                          dgroup.leave()
                    }
                    }
                    else
                    {
                          dgroup.leave()
                    }
                })
            }
            dgroup.notify(queue: DispatchQueue.main) {
                query?.removeObserver(withFirebaseHandle: queryobserve!)

                completion(sparksarray)
            }
            }
            else
            {
                query?.removeObserver(withFirebaseHandle: queryobserve!)
            completion([])
            }
            
        })
        
    }
    
    func addreport(report: Report, completed: @escaping ()->Void){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Sending Report ! ")
        let reportRef = databaseRef.child("report").childByAutoId()
        reportRef.setValue(report.toAnyObject()) { (error, ref) in
            if let error = error {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Error! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                completed()
                print(error.localizedDescription)
            }else {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Spark Update Successfully ! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                let alertView = SCLAlertView()
                alertView.showSuccess("Report", subTitle: "Your report have submitted our team will review it and take action accordingly !")
                completed()
            }
        }
    }
    
    
    func fetchAllSparksbycategory(category : String , completion: @escaping([Spark])->Void){
        
        var categorysearch = category.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
        categorysearch = "category-"+categorysearch
        databaseRef.child("spark").queryOrdered(byChild:  categorysearch).queryEqual(toValue: true).observeSingleEvent(of: .value, with: { (snapshot) in

            
            var resultArray = [Spark]()
            let dgroup: DispatchGroup = DispatchGroup()
            for sparksnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                var spark = Spark(snapshot: sparksnap)
                
                let UserRef = self.databaseRef.child("users").child(spark.userId!)
                
                UserRef.observeSingleEvent(of: .value, with: { (User) in
                    
                    let user: Useri = Useri(snapshot: User)!
                    
                    spark.profileptictureUrl = user.profilePictureUrl
                    spark.username = user.fullname
                    spark.SparkUser = user
                    self.fetchjoinedspark(joindsparkid: spark.sparkId! + "-" + (Auth.auth().currentUser?.uid)!) { (joinedspark) in
                        if(joinedspark != nil)
                        {
                            spark.joinstatus = (joinedspark?.status)!
                            spark.heartstatus = (joinedspark?.heartstatus)!
                        }
                        else
                        {
                            spark.joinstatus = false
                            spark.heartstatus = false
                            
                        }
                        resultArray.append(spark)
                        dgroup.leave()
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                    
                }
                
                
                
                
            }
            dgroup.notify(queue: DispatchQueue.main) {
                completion(resultArray)
            }
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: (error.localizedDescription))
            print(error.localizedDescription)
        }
    }
    
    
    func deletecomment(commentid: String,sparkId:String, completed: @escaping ()->Void){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Deleting ! ")
        databaseRef.child("comment").child(commentid).removeValue { (error, ref) in
            if error != nil {
                print("error \(error)")
            }
            else
            {
                
                
                let dgroup: DispatchGroup = DispatchGroup()
                dgroup.enter()
                
                let sparkRef = self.databaseRef.child("spark").child(sparkId)
                sparkRef.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                    if var data = currentData.value as? [String: Any] {
                        
                        dgroup.leave()
                        
                        var count = data["commentcount"] as? Int ?? 0
                        
                        
                        count -= 1
                        data["commentcount"] = count
                        
                        currentData.value = data
                    }
                    return TransactionResult.success(withValue: currentData)
                }
                
                dgroup.notify(queue: DispatchQueue.main) {
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Deleted ! ")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    completed()
                }
                
            
            }
        }
    
    }
    
    func deletespark(sparkId: String, completed: @escaping ()->Void){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Deleting ! ")
        databaseRef.child("spark").child(sparkId).removeValue { (error, ref) in
            if error != nil {
                print("error \(error)")
            }
            else
            {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Deleted ! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                completed()
                
                
               
            }
        }
        
    }
    
    func forgotpassword(email : String ){
        if(email != "")
        {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Processing ")
        Auth.auth().sendPasswordReset(withEmail: email) { error in
             NVActivityIndicatorPresenter.sharedInstance.setMessage("...")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if error != nil {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
            }
            else
            {
                let alertView = SCLAlertView()
                alertView.showSuccess("Email", subTitle: "Please Check your Email .")
                
            }
        }
        }
        

    }
    
    func postToken(Token: AnyObject) {
        let currentUser = Auth.auth().currentUser!
        
        databaseRef.child("users").child(currentUser.uid).child("fcmToken").setValue(Token)
    }
    
    
    func addnotification(object: Notifications, completed: @escaping ()->Void){
        let reportRef = databaseRef.child("notification").childByAutoId()
        reportRef.setValue(object.toAnyObject()) { (error, ref) in
            if let error = error {
                completed()
                print(error.localizedDescription)
            }else {
                completed()
            }
        }
    }
    
    func addblockuser(uid: String, completed: @escaping ()->Void){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Blocking User! ")
        let currentUser = Auth.auth().currentUser!
        databaseRef.child("users").child(currentUser.uid).child("blockeduser").child(uid).setValue(true) { (error, ref) in
            if let error = error {
                completed()
                print(error.localizedDescription)
            }else {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("User Blocked ! ")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                let alertView = SCLAlertView()
                alertView.showSuccess("Block User", subTitle: "User has Blocked !")
                let result = UserDefaults.standard.value(forKey: "blockusers")
                if var blocked_array = result as? Array<String> {
                    blocked_array.append(uid)
                    UserDefaults.standard.set(blocked_array, forKey: "blockusers")
                }
                else
                {
                    
                    UserDefaults.standard.set([uid], forKey: "blockusers")
                }
                
               
                completed()
                
            }
        }
        
    }
    
    func isblockedspark(sparks : [Spark] )->[Spark]
    {
        let result = UserDefaults.standard.value(forKey: "blockusers")
        if let blocked_array = result as? Array<String> {
            var filteredsparks = [Spark]()
            for item in sparks {
                if blocked_array.contains(item.userId!) {
                    
                }
                else
                {
                    filteredsparks.append(item)
                }
            }
            return filteredsparks
            
        
        }
        else
        {
            return sparks
        }
        
    }
    
    func isblockeduser(users : [Useri] )->[Useri]
    {
        let result = UserDefaults.standard.value(forKey: "blockusers")
        if let blocked_array = result as? Array<String> {
            var filteredusers = [Useri]()
            for item in users {
                if blocked_array.contains(item.uid!) {
                    
                }
                else
                {
                    filteredusers.append(item)
                }
            }
            return filteredusers
            
            
        }
        else
        {
            return users
        }
        
    }
    
    func isblockeduid(uid : String )->Bool
    {
        let result = UserDefaults.standard.value(forKey: "blockusers")
        if let blocked_array = result as? Array<String> {
            
                if blocked_array.contains(uid) {
                    return true
                }
                else
                {
                   return false
                }
            }
        else
        {
            return false
        }
            
            
    }
    
    
    func isblockedcomment(comments : [Comments] )->[Comments]
    {
        let result = UserDefaults.standard.value(forKey: "blockusers")
        if let blocked_array = result as? Array<String> {
            var filteredcomments = [Comments]()
            for item in comments {
                if blocked_array.contains(item.userId!) {
                    
                }
                else
                {
                    filteredcomments.append(item)
                }
            }
            return filteredcomments
            
        }
        else
        {
            return comments
        }
        
    }
    

    

    
    
}
