//
//  ChatViewController.swift
//  WhatsAppClone
//
//  Created by Frezy Stone Mboumba on 7/13/16.
//  Copyright © 2016 Frezy Stone Mboumba. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import MobileCoreServices
import AVKit
import SCLAlertView
import SDWebImage


class ChatViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var chatRoomId: String!
    var netService = NetworkingService()
    var messages = [JSQMessage]()
    
    var user1:Useri?
    var user2:Useri?
    
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    var databaseRef: DatabaseReference! {
        return Database.database().reference()
    }
    
    var storageRef: Storage!{
        return Storage.storage()
    }
    
    var userIsTypingRef: DatabaseReference!
    
    fileprivate var localTyping: Bool = false
    
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        print(self.senderId)
        print(self.senderDisplayName)
        
        observeTypingUser()
        
        self.title = "MESSAGES"
        let factory = JSQMessagesBubbleImageFactory()
        
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: UIColor.init(colorWithHexValue: "#EBEDEA"))
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: UIColor.init(colorWithHexValue: "#F68098"))
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
       // collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
       // collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        //collectionView.backgroundView = UIImageView(image: UIImage(named: "whatsapp-bg.jpg")!)
        
        fetchMessages()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    func fetchMessages(){
        
        let messageQuery = databaseRef.child("ChatRooms").child(chatRoomId).child("Messages").queryLimited(toLast: 30)
        messageQuery.observe(.childAdded, with: { (snapshot) in
            
            let senderId = (snapshot.value as! NSDictionary)["senderId"] as? String
            let text = (snapshot.value as! NSDictionary)["text"] as? String
            let displayName = (snapshot.value as! NSDictionary)["username"] as? String
            let mediaType = (snapshot.value as! NSDictionary)["mediaType"] as! String
            let mediaUrl = (snapshot.value as! NSDictionary)["mediaUrl"] as? String
            let date = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
            switch mediaType {
                
            case "TEXT":
                
                self.messages.append(JSQMessage(senderId: senderId, senderDisplayName: displayName, date: NSDate(timeIntervalSince1970: TimeInterval(date!)) as Date!, text: text))
               // JSQMessage(senderId: senderId, senderDisplayName: displayName, date: NSDate(timeIntervalSince1970: TimeInterval(date!)) as Date!, text: text)
            case "PHOTO":
                
                let picture = UIImage(data: try! Data(contentsOf: URL(string: mediaUrl!)!))
                let photo = JSQPhotoMediaItem(image: picture)
                self.messages.append(JSQMessage(senderId: senderId, senderDisplayName: displayName, date: NSDate(timeIntervalSince1970: TimeInterval(date!)) as Date!, media: photo))
                
                
            case "VIDEO":
                
                if let url = URL(string: mediaUrl!) {
                    let video = JSQVideoMediaItem(fileURL: url, isReadyToPlay: true)
                    self.messages.append(JSQMessage(senderId: senderId, senderDisplayName: displayName, date: NSDate(timeIntervalSince1970: TimeInterval(date!)) as Date!, media: video))
                    
                }
                
            default: break
            }
            
            self.finishReceivingMessage()
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: error.localizedDescription)
        }
        
        
    }
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        
        isTyping = textView.text != ""
    }
    
    fileprivate func observeTypingUser(){
        let typingRef = databaseRef.child("ChatRooms").child(chatRoomId).child("typingIndicator")
        userIsTypingRef = typingRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        
        let userIsTypingQuery = typingRef.queryOrderedByValue().queryEqual(toValue: true)
        
        userIsTypingQuery.observe(.value, with: { (snapshot) in
            
            if snapshot.childrenCount == 1 && self.isTyping {
                return
            }
            self.showTypingIndicator = snapshot.childrenCount > 0
            self.scrollToBottom(animated: true)
            
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: error.localizedDescription)
        }
        
        
    }
    
    
    
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let message = messages[indexPath.item]
        
        if message.isMediaMessage {
            if let media = message.media as? JSQVideoMediaItem {
                let player = AVPlayer(url: media.fileURL)
                let avPlayerViewController = AVPlayerViewController()
                avPlayerViewController.player = player
                self.present(avPlayerViewController, animated: true, completion: nil)
                
            }
        }
    }
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let messageRef = databaseRef.child("ChatRooms").child(chatRoomId).child("Messages").childByAutoId()
        let message = Message(text: text, senderId: senderId, username: senderDisplayName, mediaType: "TEXT", mediaUrl: "",postDate : NSDate().timeIntervalSince1970 as NSNumber)
        
        messageRef.setValue(message.toAnyObject()) { (error, ref) in
            if error == nil {
                
                let lastMessageRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("lastMessage")
                lastMessageRef.setValue(text, withCompletionBlock: { (error, ref) in
                    if error == nil {
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateDiscussions"), object: nil)
                        
                    }else {
                        let alertView = SCLAlertView()
                        alertView.showError("OOPS", subTitle: error!.localizedDescription)
                        
                    }
                    self.netService.addnotification(object: Notifications(identifier: "message", object: message.toAnyObject(),extrastring: self.chatRoomId), completed: {
                    })
                    
                })
                let lastTimeRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("date")
                lastTimeRef.setValue(Date().timeIntervalSince1970, withCompletionBlock: { (error, ref) in
                    if error == nil {
                        
                    }else {
                        let alertView = SCLAlertView()
                        alertView.showError("OOPS", subTitle: error!.localizedDescription)
                        
                    }
                })
                
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                self.finishSendingMessage()
                
            }else {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: error!.localizedDescription)
            }
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        let alertController = UIAlertController(title: "Medias", message: "Choose your media type", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        
        let imageAction = UIAlertAction(title: "Image", style: UIAlertActionStyle.default) { (action) in
            self.getMedia(kUTTypeImage)
            
        }
        
        let videoAction = UIAlertAction(title: "Video", style: UIAlertActionStyle.default) { (action) in
            self.getMedia(kUTTypeMovie)
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        alertController.addAction(imageAction)
        alertController.addAction(videoAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let picture = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            
            self.saveMediaMessage(withImage: picture, withVideo: nil)
            
            
        } else if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            
            self.saveMediaMessage(withImage: nil, withVideo: videoUrl)
            
        }
        
        self.dismiss(animated: true) {
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            self.finishSendingMessage()
            
        }
        
    }
    
    fileprivate func saveMediaMessage(withImage image: UIImage?, withVideo: URL?){
        
        if let image = image {
            
            let imagePath = "messageWithMedia\(chatRoomId + UUID().uuidString)/photo.jpg"
            
            let imageRef = storageRef.reference().child(imagePath)
            
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            let imageData = UIImageJPEGRepresentation(image, 0.2)!
            
            imageRef.putData(imageData, metadata: metadata, completion: { (newMetaData, error) in
                
                if error == nil {
                    
                    let message = Message(text: "", senderId: self.senderId, username: self.senderDisplayName, mediaType: "PHOTO", mediaUrl: String(describing: newMetaData!.downloadURL()!),postDate: NSDate().timeIntervalSince1970 as NSNumber)
                    let messageRef =  self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("Messages").childByAutoId()
                    
                    messageRef.setValue(message.toAnyObject(), withCompletionBlock: { (error, ref) in
                        if error == nil {
                            
                            let lastMessageRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("lastMessage")
                            lastMessageRef.setValue(String(describing: newMetaData!.downloadURL()!), withCompletionBlock: { (error, ref) in
                                if error == nil {
                                    
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "updateDiscussions"), object: nil)
                                    
                                }else {
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                                    
                                }
                                
                                
                            })
                            let lastTimeRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("date")
                            lastTimeRef.setValue(Date().timeIntervalSince1970, withCompletionBlock: { (error, ref) in
                                if error == nil {
                                    
                                }else {
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                                    
                                }
                            })
                            
                            JSQSystemSoundPlayer.jsq_playMessageSentSound()
                            self.finishSendingMessage()
                            
                        }
                        
                    })
                    
                    
                }else {
                    let alertView = SCLAlertView()
                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                }
            })
            
            
        } else {
            
            
            let videoPath = "messageWithMedia\(chatRoomId + UUID().uuidString)/video.mp4"
            
            let videoRef = storageRef.reference().child(videoPath)
            
            let metadata = StorageMetadata()
            metadata.contentType = "video/mp4"
            
            let videoData = try! Data(contentsOf: withVideo!)
            
            videoRef.putData(videoData, metadata: metadata, completion: { (newMetaData, error) in
                
                if error == nil {
                    
                    let message = Message(text: "", senderId: self.senderId, username: self.senderDisplayName, mediaType: "VIDEO", mediaUrl: String(describing: newMetaData!.downloadURL()!),postDate:NSDate().timeIntervalSince1970 as NSNumber)
                    
                    let messageRef =  self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("Messages").childByAutoId()
                    
                    messageRef.setValue(message.toAnyObject(), withCompletionBlock: { (error, ref) in
                        if error == nil {
                            
                            let lastMessageRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("lastMessage")
                            lastMessageRef.setValue(String(describing: newMetaData!.downloadURL()!), withCompletionBlock: { (error, ref) in
                                if error == nil {
                                    
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "updateDiscussions"), object: nil)
                                    
                                }else {
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                                    
                                }
                                
                                
                            })
                            let lastTimeRef = self.databaseRef.child("ChatRooms").child(self.chatRoomId).child("date")
                            lastTimeRef.setValue(Date().timeIntervalSince1970, withCompletionBlock: { (error, ref) in
                                if error == nil {
                                    
                                }else {
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                                    
                                }
                            })
                            
                            JSQSystemSoundPlayer.jsq_playMessageSentSound()
                            self.finishSendingMessage()
                            
                        }
                        
                    })
                    
                }else {
                    let alertView = SCLAlertView()
                    alertView.showError("OOPS", subTitle: error!.localizedDescription)
                }
            })
            
            
            
            
            
            
        }
        
        
    }
    
    fileprivate func getMedia(_ mediaType: CFString){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.isEditing = true
        
        if mediaType == kUTTypeImage {
            
            imagePicker.mediaTypes = [mediaType as String]
            
        } else if mediaType == kUTTypeMovie {
            
            imagePicker.mediaTypes = [mediaType as String]
            
        }
        
        present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        }else {
            return incomingBubbleImageView
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.item]
            if message.senderId == senderId {
              
                let placeHolderImage = UIImage(named: "default")
                let avatarImage = JSQMessagesAvatarImage(avatarImage: nil, highlightedImage: nil, placeholderImage: placeHolderImage)
                avatarImage?.avatarImage = JSQMessagesAvatarImageFactory.circularAvatarImage(placeHolderImage, withDiameter: 60)
                return avatarImage
            }
            else
            {
                let placeHolderImage = UIImage(named: "default")
                let avatarImage = JSQMessagesAvatarImage(avatarImage: nil, highlightedImage: nil, placeholderImage: placeHolderImage)
                avatarImage?.avatarImage = JSQMessagesAvatarImageFactory.circularAvatarImage(placeHolderImage, withDiameter: 60)
                return avatarImage
            }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if !message.isMediaMessage {
            if message.senderId == senderId {
                cell.textView.textColor = UIColor.white
                 cell.avatarImageView.sd_setImage(with: URL(string: (self.user1?.profilePictureUrl)!), placeholderImage: UIImage(named: "default"))
                cell.avatarImageView.contentMode = .scaleAspectFill
                cell.avatarImageView.clipsToBounds = true
                //let rad = cell.avatarImageView.bounds.width/2
                cell.avatarImageView.layer.cornerRadius = 15
                
            }else {
                cell.textView.textColor = UIColor.darkGray
                 cell.avatarImageView.sd_setImage(with: URL(string: (self.user2?.profilePictureUrl)!), placeholderImage: UIImage(named: "default"))
                cell.avatarImageView.contentMode = .scaleAspectFill
                cell.avatarImageView.clipsToBounds = true
               // let rad = cell.avatarImageView.bounds.width/2
                cell.avatarImageView.layer.cornerRadius = 15
                cell.textView.font = UIFont(name: "Helvetica Neue", size: 14.0)
            }
        }
        
       
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        let message = messages[indexPath.row]
        let data = self.messages[(indexPath as NSIndexPath).row]
        
        if (self.senderDisplayName == data.senderDisplayName) {
            return nil
        }
        if indexPath.row > 1 {
            let previousMessage = messages[indexPath.row - 1]
            if previousMessage.senderId == message.senderId {
                return nil
            }
        }
        return NSAttributedString(string: data.senderDisplayName!)
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = self.messages[indexPath.item]
        if indexPath.row == 0 {
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        if indexPath.row - 1 > 0 {
            let previousMessage = messages[indexPath.row - 1]
            if message.date.timeIntervalSince(previousMessage.date as Date) / (60*5) > 1 {
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
            }
        }
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.row == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        if indexPath.row - 1 > 0 {
            let previousMessage = self.messages[indexPath.row - 1]
            let message = self.messages[indexPath.row]
            if message.date.timeIntervalSince(previousMessage.date as Date) / (60*5) > 1 {
                return kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        return 0.0
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        
        let message = messages[indexPath.row]
        
        let data = self.messages[(indexPath as NSIndexPath).row]
        if (self.senderDisplayName == data.senderDisplayName) {
            return 0.0
        }
        if indexPath.row > 1 {
            let previousMessage = self.messages[indexPath.row - 1]
            if previousMessage.senderId == message.senderId {
                return 0.0
            }
        }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
   
    
    
    
    
    
    
}
