//
//  SearchResultsController.swift
//  PlacesLookup
//
//  Created by Malek T. on 9/30/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import SCLAlertView
import FBSDKShareKit



class UserSearchResultsController: UIViewController ,UITableViewDelegate ,UITableViewDataSource ,UISearchControllerDelegate , SparkTableViewCellDelegate {
    
    
    // MARK: - Properties
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var MySegmentcontrol: UISegmentedControl!
    var searchResults: [Useri] = []
    var SparkArray: [Spark] = []
    var TagSparkArray: [Spark] = []
    var tableempty : Bool = false
    var chatFunctions = ChatFunctions()
    let searchController = UISearchController(searchResultsController: nil)
      var netService = NetworkingService()
    var selectedindex:Int?
    // MARK: - View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview: (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.shadowImage = UIImage()
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.delegate = self
        self.definesPresentationContext = true
       
      // tableView.tableHeaderView = searchController.searchBar
        navigationItem.hidesBackButton = true
//        if #available(iOS 11.0, *) {
//            searchController.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
//        }
        self.navigationItem.titleView = searchController.searchBar
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }
        
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.isActive = true
        self.MySegmentcontrol.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.lightGray], for: UIControlState.selected)
        self.MySegmentcontrol.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.normal)
        for subView in searchController.searchBar.subviews {
            
            for subViewOne in subView.subviews {
                
                if let textField = subViewOne as? UITextField {
                    subViewOne.backgroundColor = UIColor(white: 1, alpha: 0.2)
                    
                    //use the code below if you want to change the color of placeholder
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = UIColor.lightGray
                }
            }
        }
        MySegmentcontrol.removeBorders()
        MySegmentcontrol.backgroundColor = UIColor.init(colorWithHexValue: "#A0C8EE")
        MySegmentcontrol.setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
      MySegmentcontrol.addTarget(self, action: #selector(segmentSelected(sender:)), for: .valueChanged)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in }) { (completed) -> Void in
            
            self.searchController.searchBar.becomeFirstResponder()
        }
        
        if(selectedindex != nil)
        {
            MySegmentcontrol.selectedSegmentIndex = selectedindex!
            filterContentForSearchText(searchController.searchBar.text!)
            selectedindex = 0
        }
        
        
    }
    
    func segmentSelected(sender: UISegmentedControl)
    {
        let index = sender.selectedSegmentIndex
        print(index)
        filterContentForSearchText(searchController.searchBar.text!)
        // Do what you want
    }
    
    func didPresentSearchController(_ searchController: UISearchController){
        UIView.animate(withDuration: 0.1, animations: { () -> Void in }) { (completed) -> Void in
            
            self.searchController.searchBar.becomeFirstResponder()
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if let navController = self.navigationController {
            let transition: CATransition = CATransition()
            transition.duration = 1
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionFade
            self.navigationController!.view.layer.add(transition, forKey: nil)
            navController.popViewController(animated: false)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
  
  
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int = 0
        if(MySegmentcontrol.selectedSegmentIndex == 1 )
        {
          count =  self.searchResults.count
        }
        else if(MySegmentcontrol.selectedSegmentIndex == 0 )
        {
          count =  self.SparkArray.count
        }
        else if(MySegmentcontrol.selectedSegmentIndex == 2 )
        {
            count =  self.TagSparkArray.count
        }
        
        if(count == 0)
        {
            tableempty = true
            count = 1
        }
        else
        {
            tableempty = false
        }
        
        
        return count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(!tableempty)
        {
        if(MySegmentcontrol.selectedSegmentIndex == 1 )
        {
        var cell: UserTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as? UserTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as? UserTableViewCell
        }
            cell?.delegate = self
        return cell!
        }
        else
        {
            var cell: SparkTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
            if cell == nil {
                tableView.register(UINib(nibName: "SparkTableViewCell", bundle: nil), forCellReuseIdentifier: "SparkTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "SparkTableViewCell") as? SparkTableViewCell
                
            }
            
           cell?.delegate = self
            cell?.tag = indexPath.row
            return cell!
        }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
            if((searchController.searchBar.text?.characters.count)! > 3)
            {
                cell.textLabel?.text = "No result !"
                cell.textLabel?.textColor = UIColor.darkGray
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 )
        {
            return 20
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(!tableempty)
        {
        if(MySegmentcontrol.selectedSegmentIndex == 1 )
        {
        
          if let userCell = cell as? UserTableViewCell  {
     
            userCell.configureCell(User: self.searchResults[indexPath.row])
            
          }
        }
        else if(MySegmentcontrol.selectedSegmentIndex == 0 )
        {
            if let sparkCell = cell as? SparkTableViewCell  {
                
                
                sparkCell.configureCell(Spark: self.SparkArray[indexPath.row])
                
                
            }
        }
        else if(MySegmentcontrol.selectedSegmentIndex == 2 )
        {
            if let sparkCell = cell as? SparkTableViewCell  {
                
                
                sparkCell.configureCell(Spark: self.TagSparkArray[indexPath.row])
                
                
            }
        }
        }
        
        
    }
    
   
    
    func filterContentForSearchText(_ searchText: String) {
        if(searchText.characters.count >= 3)
        {
           if(MySegmentcontrol.selectedSegmentIndex == 1 )
           {
            netService.SearchAllUsers(textstring: searchText.lowercased() , completion: { (users) in
                self.searchResults = users
                self.searchResults = self.netService.isblockeduser(users:self.searchResults)
                self.tableView.reloadData()
            })
            
           }
            
            if(MySegmentcontrol.selectedSegmentIndex == 0 )
            {
                netService.SearchAllSparks(textstring: searchText.lowercased() , completion: { (sparks) in
                    self.SparkArray = sparks
                    self.SparkArray = self.netService.isblockedspark(sparks: self.SparkArray)
                    self.tableView.reloadData()
                })
                
            }
            if(MySegmentcontrol.selectedSegmentIndex == 2 )
            {
                netService.SearchAllTagSparks(textstring: searchText.lowercased() , completion: { (sparks) in
                    self.TagSparkArray = sparks
                    self.TagSparkArray = self.netService.isblockedspark(sparks: self.TagSparkArray)
                    self.tableView.reloadData()
                })
                
            }
          
            
        }
        else
        {
             self.tableView.reloadData()
        }
        
    }
    
    // MARK: - Segues
    
    func didtapviewbutton(Spark: Spark, Cell: UITableViewCell) {
        print("here")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
        SparkdetailsViewController.thisspark = Spark
        self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
    }
    
    func didtapjoinbutton(Spark: Spark, Cell: SparkTableViewCell, custom : Any?,sender:Any?) {
        print("here")
        if(custom != nil)
        {
            print("")
            if(custom as! String == "comments")
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let CommentViewController = storyBoard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
                CommentViewController.thisspark = Spark
                self.navigationController?.pushViewController(CommentViewController, animated: true)
            }
            if(custom as! String == "option")
            {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                var action_title : String = "Report"
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    action_title = "Edit"
                }
                //First action
                let actionOne = UIAlertAction(title: action_title, style: .default) { (action) in
                    if(Spark.userId == Auth.auth().currentUser?.uid )
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let UpdateSparkViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateSparkViewController") as! UpdateSparkViewController
                        UpdateSparkViewController.spark = Spark
                        self.navigationController?.pushViewController(UpdateSparkViewController, animated: true)
                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                        ReportViewController.reportitem = "Spark"
                        ReportViewController.reportitemid = Spark.sparkId!
                        self.navigationController?.pushViewController(ReportViewController, animated: true)
                    }
                }
                 alert.addAction(actionOne)
                if(Spark.userId == Auth.auth().currentUser?.uid )
                {
                    print("delete")
                    let action_delete_title : String = "Delete"
                    let actiondelete = UIAlertAction(title: action_delete_title, style: .default) { (action) in
                        self.netService.deletespark(sparkId: Spark.sparkId!, completed: {
                            //code
                            self.SparkArray.remove(at: Cell.tag)
                            self.tableView.reloadData()
                        })
                    }
                    alert.addAction(actiondelete)
                }
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
               
                alert.addAction(actionThree)
                
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                }
                //Present alert
                self.present(alert, animated: true, completion: nil)
            }
            if(custom as! String == "share")
            {
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                //First action
                let actionOne = UIAlertAction(title: "Facebook", style: .default) { (action) in
                    DispatchQueue.main.async {
                        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                        
                        content.quote = (Spark.title?.uppercased())! + " : " + Spark.description!
                        content.contentURL = NSURL(string: "https://t6b9g.app.goo.gl/V9Hh")! as URL
                        
                        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
                    }
                }
                
                //Second action
                let actionTwo = UIAlertAction(title: "Twitter", style: .default) { (action) in
                    DispatchQueue.main.async {
                        //Checking if user is connected to Facebook
                        
                        if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                            // App must have at least one logged-in user to compose a Tweet
                            let composer = TWTRComposer()
                            
                            composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                            //composer.setImage(UIImage(named: "twitterkit"))
                            
                            // Called from a UIViewController
                            
                            composer.show(from: self.navigationController!, completion: { (result) in
                                if (result == .done) {
                                    print("Successfully composed Tweet")
                                } else {
                                    print("Cancelled composing")
                                }
                            })
                            // self.present(composer, animated: true, completion: nil)
                        } else {
                            // Log in, and then check again
                            Twitter.sharedInstance().logIn { session, error in
                                if session != nil { // Log in succeeded
                                    let composer = TWTRComposer()
                                    
                                    composer.setText((Spark.title?.uppercased())! + " : " + Spark.description!+"  https://t6b9g.app.goo.gl/V9Hh")
                                    //composer.setImage(UIImage(named: "twitterkit"))
                                    
                                    // Called from a UIViewController
                                    
                                    composer.show(from: self.navigationController!, completion: { (result) in
                                        if (result == .done) {
                                            print("Successfully composed Tweet")
                                        } else {
                                            print("Cancelled composing")
                                        }
                                    })
                                } else {
                                    
                                    let alertView = SCLAlertView()
                                    alertView.showError("OOPS", subTitle: "No Twitter Accounts Available Or Error in completing request")
                                }
                            }
                        }
                    }
                }
                
                
                let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                //Add action to action sheet
                alert.addAction(actionOne)
                alert.addAction(actionTwo)
                alert.addAction(actionThree)
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = sender as? UIView
                    popoverController.sourceRect = (sender as AnyObject).bounds;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
                }
                //Present alert
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func showAlert(service:String)
    {
        
        let alertView = SCLAlertView()
        alertView.showError("OOPS", subTitle: "You are not connected to \(service)")
    }

    
    func didtapprofilebutton(user : Useri)
    {

        
        
        if let action = user.action {
            
            if(action == "chat")
            {
                var inituserdic = [String: Any]()
                inituserdic["fullname"] = Auth.auth().currentUser!.displayName!
                inituserdic["uid"] = Auth.auth().currentUser!.uid
                inituserdic["profilePictureUrl"] = String(describing: Auth.auth().currentUser!.photoURL!)
                let currentUser = Useri.init(initdic: inituserdic)
                chatFunctions.startChat(currentUser, user2: user)
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let chatVC = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatVC.senderId = Auth.auth().currentUser!.uid
                chatVC.senderDisplayName = Auth.auth().currentUser!.displayName!
                chatVC.chatRoomId = chatFunctions.chatRoom_id
                chatVC.user1 = currentUser
                chatVC.user2 = user
                self.navigationController?.pushViewController(chatVC, animated: true)
            }
        }
        else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let ProfileViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            ProfileViewController.thisuser = user
            ProfileViewController.navigationItem.leftBarButtonItem = nil
            ProfileViewController.navigationItem.rightBarButtonItem = nil
            self.navigationController?.pushViewController(ProfileViewController, animated: true)

        }
        

        

       
    }
    
    
}

extension UserSearchResultsController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
       // filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension UserSearchResultsController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
       // let searchBar = searchController.searchBar
       // let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
