//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FBSDKLoginKit
import GoogleSignIn
import SCLAlertView
import NVActivityIndicatorView

class Signin: UIViewController , UITextFieldDelegate ,GIDSignInUIDelegate {

   

    var networkingService = NetworkingService()
    
    @IBOutlet weak var emailfield: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    
    let activityData = ActivityData.init(size: nil, message: "Loading ..", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setTapGestureRecognizerOnView()
        setSwipeGestureRecognizerOnView()
        assignbackground()
        addfields()
       
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    
    @IBAction func forgotpassword(_ sender: Any) {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Forgot Password", message: "Enter Your Email", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            //print("Text field: \(String(describing: textField?.text))")
            
            self.networkingService.forgotpassword(email: (textField?.text)!)
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
 
    
    @IBAction func Signin(_ sender: Any) {
       
        let email = emailfield?.text!.lowercased()
        let finalEmail = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = passwordField?.text!
        
        if (finalEmail?.isEmpty)! || (password?.isEmpty)! {
            print("fields required")
        }else {
            if isValidEmail(email: finalEmail!) {
                self.networkingService.signIn(email: finalEmail!, password: password!)
            }
        }
        self.view.endEditing(true)
    }
    
    
    @IBAction func Signup(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Signup") as! Signup
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @IBAction func Facebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.networkingService.facebookSignup()
                        //fbLoginManager.logOut()
                    }
                }
            }
            else
            {
                let alertView = SCLAlertView()
                alertView.showError("OOPS", subTitle: (error?.localizedDescription)!)
            }
        }
        
    }
    
    
    
    @IBAction func Google(_ sender: Any) {
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        //NVActivityIndicatorPresenter.sharedInstance.setMessage("Requesting Google ! ")
        GIDSignIn.sharedInstance().signIn()
        
    }
    
  
    
    
    func addfields()
    {
        
       
        if let emailfield = emailfield {
             emailfield.placeholder = "EMAIL"
             emailfield.title = "EMAIL"
            emailfield.errorColor = UIColor.red
             emailfield.delegate = self
        }
        
        
        if let passwordField = passwordField {
        passwordField.placeholder = "PASSWORD"
        passwordField.title = "PASSWORD"
            passwordField.delegate = self
        }
        
       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
              
                if(floatingLabelTextField.placeholder == "EMAIL")
                {
                if(text.characters.count < 3 || !text.contains("@")) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
                }
            }
        }
        return true
    }
    
    func assignbackground(){
        let background = UIImage(named: "bg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    @IBAction func termscondition(_ sender: Any) {
        guard let url = URL(string: "http://wildfiresconnect.com/index.php/terms-conditions/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    
        

}





extension Signin {
    
   

   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailfield?.resignFirstResponder()
        passwordField?.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateView(up: true, moveValue: 80)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateView(up: false, moveValue:
            80)
    }
    
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    func setSwipeGestureRecognizerOnView(){
        let swipDown = UISwipeGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        swipDown.direction = .down
        self.view.addGestureRecognizer(swipDown)
    }
}

