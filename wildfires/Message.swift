//
//  Message.swift
//  WhatsAppClone
//
//  Created by Frezy Stone Mboumba on 8/27/16.
//  Copyright © 2016 Frezy Stone Mboumba. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct Message {
    
    var text: String!
    var senderId: String!
    var username: String!
    var mediaType: String!
    var mediaUrl: String!
    var postDate: NSNumber!
    var ref: DatabaseReference!
    var key: String = ""
    
    
    init(snapshot: DataSnapshot){
        
        
        self.text = (snapshot.value as! NSDictionary)["text"] as? String
        self.senderId = (snapshot.value as! NSDictionary)["senderId"] as? String
        self.username = (snapshot.value as! NSDictionary)["username"] as? String
        self.mediaType = (snapshot.value as! NSDictionary)["mediaType"] as? String
        self.mediaUrl = (snapshot.value as! NSDictionary)["mediaUrl"] as? String
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber
        self.ref = snapshot.ref
        self.key = snapshot.key
        
    }
    
    
    init(text: String, key: String = "", senderId: String, username: String, mediaType: String, mediaUrl: String,postDate : NSNumber){
        
        
        self.text = text
        self.senderId = senderId
        self.username = username
        self.mediaUrl = mediaUrl
        self.mediaType = mediaType
        self.postDate = postDate
    }
    
    
    func toAnyObject() -> [String: AnyObject]{
        
        return ["text": text as AnyObject,"senderId": senderId as AnyObject, "username": username as AnyObject,"mediaType":mediaType as AnyObject, "mediaUrl":mediaUrl as AnyObject , "postDate":postDate as AnyObject]
    }
    
    
    
}
