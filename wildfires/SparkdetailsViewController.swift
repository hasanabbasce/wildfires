//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import Firebase
import SCLAlertView
import FBSDKShareKit

class SparkdetailsViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource,CommentTableViewCellDelegate {
    
    
    @IBOutlet weak var coverimage: CustomizableImageView!
    @IBOutlet weak var sparktitle: UILabel!
    @IBOutlet weak var userprofileimage: circularimage!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var sparklocation: UILabel!
    @IBOutlet weak var catogories: UILabel!
    @IBOutlet weak var sparkdescription: UILabel!
    @IBOutlet weak var commentbox: UITextView!
    @IBOutlet weak var joinbtn: UIButton!
    @IBOutlet weak var likebutton: UIButton!
    @IBOutlet weak var sharebtn: UIButton!
    
    @IBOutlet weak var countdetails: UILabel!
    
    @IBOutlet weak var addeventheight: NSLayoutConstraint!
    
    
    
   
   // var sparkid : String?
    var commentsArray = [Comments]()
    var eventArray = [Event]()
    var thisspark : Spark?
    
    var netService = NetworkingService()
    
    @IBOutlet weak var commenttableheightcontrin: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var addeventbtn: UIButton!
    @IBOutlet weak var eventlable: UILabel!
    
    @IBOutlet weak var eventtable: UITableView!
    @IBOutlet weak var eventtableheightcontrin: NSLayoutConstraint!
    
    @IBOutlet weak var eventlableheight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //sparkid = "6B341374-CBBF-45FE-81C6-EDD182080500"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        setTapGestureRecognizerOnView()
        commentbox!.layer.borderWidth = 1
        commentbox!.layer.borderColor = UIColor.lightGray.cgColor
        commentbox.delegate = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.delegate = self
        tableView.dataSource = self
        
        eventtable.separatorStyle = UITableViewCellSeparatorStyle.none
        eventtable.delegate = self
        eventtable.dataSource = self
        
         self.sharebtn.tintColor = UIColor.lightGray
       // self.tableView.rowHeight = UITableViewAutomaticDimension
       // self.tableView.estimatedRowHeight = 250
        
        
        renderSpark()
        
        self.commentbox.resolveHashTags()
        
    }
    
    private func renderSpark()
    {
        self.coverimage.sd_setImage(with: URL(string: (thisspark?.cover!)!), placeholderImage: UIImage(named: "default"))
        self.sparktitle.text = thisspark?.title
        self.userprofileimage.sd_setImage(with: URL(string: (thisspark?.profileptictureUrl!)!), placeholderImage: UIImage(named: "default"))
        self.username.text = thisspark?.username
        self.sparklocation.text = thisspark?.location
        self.catogories.text = thisspark?.catogories
        self.sparkdescription.text = thisspark?.description
        self.sparkdescription.resolveHashTags()
        
    
            if(thisspark?.joinstatus)!
            {

                self.joinbtn.setTitle(" Leave",for: .normal)
                self.joinbtn.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")

            }
            else
            {

                self.joinbtn.setTitle(" Join",for: .normal)
                self.joinbtn.tintColor = UIColor.lightGray
             
            }
            
            if(thisspark?.heartstatus)!
            {

                self.likebutton.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
                
            }
            else
            {

                self.likebutton.tintColor = UIColor.lightGray
            }
        
        var countdetailstext = ""
        var  comma = ""
        if(thisspark?.joincount != 0)
        {
            if let count = thisspark?.joincount {
                countdetailstext =  "\(count)"  + " Joins"
                comma = ","
            }
            
        }
        
        if(thisspark?.heartcount != 0)
        {
            if let count = thisspark?.heartcount {
                countdetailstext =  countdetailstext + comma +  " " + "\(count)" + " Likes"
                comma = ","
            }
        }
        
        if(thisspark?.commentcount != 0)
        {
            if let count = thisspark?.commentcount {
                countdetailstext =  countdetailstext + comma + " " + "\(count)" + " Comments"
            }
        }
        
        countdetails.text = countdetailstext
        
        
        if(Auth.auth().currentUser?.uid != thisspark?.userId)
        {
           addeventheight.constant = 0
        }

        
        rendercomments()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        renderevents()
    }
    
    @IBAction func createevent(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let CreateEventViewController = storyBoard.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        CreateEventViewController.sparkId = thisspark?.sparkId
        self.navigationController?.pushViewController(CreateEventViewController, animated: true)
        
    }
    
    
    private func rendercomments()
    {
        netService.fetchcommentsfromDB(Sparkid: (self.thisspark?.sparkId!)!) { (comments) in
        self.commentsArray = comments
        self.commentsArray.sort(by: { (comment1, comment2) -> Bool in
            Int(comment1.postDate!) > Int(comment2.postDate!)
        })
            self.commentsArray = self.netService.isblockedcomment(comments: self.commentsArray)
        self.tableView.reloadData()
        }
    }
    
    private func renderevents()
    {
        //self.eventlableheight.constant = 0
       // self.eventtableheightcontrin.constant = 0
        netService.fetcheventsfromDB(Sparkid: (self.thisspark?.sparkId!)!) { (comments) in
            self.eventArray = comments
            
            if(self.eventArray.count < 1)
            {
                self.eventlableheight.constant = 0
                self.eventtableheightcontrin.constant = 0
            }
            else
            {
                self.eventlableheight.constant = 20
                 self.eventtableheightcontrin.constant = 20
            }
            
            self.eventArray.sort(by: { (comment1, comment2) -> Bool in
                Int(comment1.postDate!) > Int(comment2.postDate!)
            })
           
            self.eventtable.reloadData()
        }
    }
    
    @IBAction func Share(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //First action
        //First action
        let actionOne = UIAlertAction(title: "Facebook", style: .default) { (action) in
            DispatchQueue.main.async {
                let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
                
                content.quote = (self.thisspark!.title?.uppercased())! + " : " + (self.thisspark?.description!)!
                content.contentURL = NSURL(string: "https://t6b9g.app.goo.gl/V9Hh")! as URL
                
                FBSDKShareDialog.show(from: self, with: content, delegate: nil)
            }
        }
        
        //Second action
        let actionTwo = UIAlertAction(title: "Twitter", style: .default) { (action) in
            DispatchQueue.main.async {
                //Checking if user is connected to Facebook
                
                if (Twitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                    // App must have at least one logged-in user to compose a Tweet
                    let composer = TWTRComposer()
                    
                    composer.setText((self.thisspark?.title?.uppercased())! + " : " + (self.thisspark?.description!)!+"  https://t6b9g.app.goo.gl/V9Hh")
                    //composer.setImage(UIImage(named: "twitterkit"))
                    
                    // Called from a UIViewController
                    
                    composer.show(from: self.navigationController!, completion: { (result) in
                        if (result == .done) {
                            print("Successfully composed Tweet")
                        } else {
                            print("Cancelled composing")
                        }
                    })
                    // self.present(composer, animated: true, completion: nil)
                } else {
                    // Log in, and then check again
                    Twitter.sharedInstance().logIn { session, error in
                        if session != nil { // Log in succeeded
                            let composer = TWTRComposer()
                            
                            composer.setText((self.thisspark?.title?.uppercased())! + " : " + (self.thisspark?.description!)!+"  https://t6b9g.app.goo.gl/V9Hh")
                            //composer.setImage(UIImage(named: "twitterkit"))
                            
                            // Called from a UIViewController
                            
                            composer.show(from: self.navigationController!, completion: { (result) in
                                if (result == .done) {
                                    print("Successfully composed Tweet")
                                } else {
                                    print("Cancelled composing")
                                }
                            })
                        } else {
                            
                            let alertView = SCLAlertView()
                            alertView.showError("OOPS", subTitle: "No Twitter Accounts Available Or Error in completing request")
                        }
                    }
                }
            }
        }
        
        let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        //Add action to action sheet
        alert.addAction(actionOne)
        alert.addAction(actionTwo)
        alert.addAction(actionThree)
        
        if let popoverController = alert.popoverPresentationController {
//            popoverController.sourceView = self.view
//            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//            popoverController.permittedArrowDirections = []
            popoverController.sourceView = sender as? UIView
            popoverController.sourceRect = (sender as AnyObject).bounds;
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
        }
        //Present alert
        self.present(alert, animated: true, completion: nil)

    }
   
    func showAlert(service:String)
    {
        
        let alertView = SCLAlertView()
        alertView.showError("OOPS", subTitle: "You are not connected to \(service)")
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        // clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    @IBAction func commentsubmit(_ sender: Any) {
        let postDate = NSDate().timeIntervalSince1970 as NSNumber
        let newComment = Comments(userId: (Auth.auth().currentUser?.uid)!, sparkId: (thisspark?.sparkId)!, comment: commentbox.text, postDate : postDate)
        self.netService.addcommentToDB(comment: newComment) {
            self.commentbox!.text = ""
        }
    }
    
    @IBAction func joinspark(_ sender: Any) {
        

                self.thisspark?.joinstatus = !(self.thisspark?.joinstatus)!
                if(thisspark?.joinstatus)!
                {

                    self.joinbtn.setTitle(" Leave",for: .normal)
                    self.joinbtn.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
              
                }
                else
                {

                    self.joinbtn.setTitle(" Join",for: .normal)
                    self.joinbtn.tintColor = UIColor.lightGray
                }
        
                let joinspark = JoinedSpark(joinedsparkId: thisspark!.sparkId! + "-" + (Auth.auth().currentUser?.uid)! , userId: (Auth.auth().currentUser?.uid)!, sparkuserId: (thisspark?.userId)!, sparkId: (thisspark?.sparkId!)! , status : (self.thisspark?.joinstatus)! , heartstatus :(self.thisspark?.heartstatus)! )
                netService.joinSpark(joinedspark: joinspark,type: "join", completed: {
                })


    }
    
    @IBAction func likeaction(_ sender: Any) {
        self.thisspark?.heartstatus = !(self.thisspark?.heartstatus)!
        if(thisspark?.heartstatus)!
        {

            self.likebutton.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
            
        }
        else
        {

            self.likebutton.tintColor = UIColor.lightGray
        }
        
        let joinspark = JoinedSpark(joinedsparkId: thisspark!.sparkId! + "-" + (Auth.auth().currentUser?.uid)! , userId: (Auth.auth().currentUser?.uid)!, sparkuserId: (thisspark?.userId)!, sparkId: (thisspark?.sparkId!)! , status : (self.thisspark?.joinstatus)! , heartstatus :(self.thisspark?.heartstatus)! )
        netService.joinSpark(joinedspark: joinspark,type: "heart", completed: {
        })
    }
    
    
    
    
     func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.tableView)
        {
            return self.commentsArray.count
        }
        else
        {
            return self.eventArray.count
        }
        
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.tableView)
        {
        var cell: CommentTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell
            
        }
        cell?.delegate = self
        cell?.tag = indexPath.row
        return cell!
        }
        else
        {
            var cell: EventTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as? EventTableViewCell
            if cell == nil {
                tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as? EventTableViewCell
                
            }
            //cell?.delegate = self
            cell?.tag = indexPath.row
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView == self.tableView)
        {
        let commentstring = self.commentsArray[indexPath.row].comment!
        let heigt = commentstring.height(withConstrainedWidth: tableView.contentSize.width, font: UIFont.systemFont(ofSize: 15))
       // print(heigt)
        return heigt+60
        }
        else
        {
          
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(tableView == self.tableView)
        {
         commenttableheightcontrin.constant = tableView.contentSize.height
        }
        else
        {
            eventtableheightcontrin.constant = eventtable.contentSize.height
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(tableView == self.tableView)
        {
        
        if let CommentCell = cell as? CommentTableViewCell  {
            CommentCell.configureCell(Comment: self.commentsArray[indexPath.row])
            
        }
         commenttableheightcontrin.constant = tableView.contentSize.height
            
        }
        else
        {
            if let EventCell = cell as? EventTableViewCell  {
                EventCell.configureCell(Event: self.eventArray[indexPath.row])
                
            }
            eventtableheightcontrin.constant = eventtable.contentSize.height
        }
        
        
    }
    
    func eventtoview(comment: Comments,Cell : UITableViewCell,action : String,sender:Any?)
    {
       if(action == "setting")
       {
         let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        var action_title : String = "Report"
        if(comment.userId == Auth.auth().currentUser?.uid )
        {
            action_title = "Delete"
        }
       
            let actiondelete = UIAlertAction(title: action_title, style: .default) { (action) in
                if(comment.userId == Auth.auth().currentUser?.uid )
                {
                self.netService.deletecomment(commentid: comment.key,sparkId:comment.sparkId!, completed: {
                    //code
                    self.commentsArray.remove(at: Cell.tag)
                    self.tableView.reloadData()
                })
                }
                else
                {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                    ReportViewController.reportitem = "Comment"
                    ReportViewController.reportitemid = comment.key
                    self.navigationController?.pushViewController(ReportViewController, animated: true)
                }
            }
            alert.addAction(actiondelete)
        
        
        let actionThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        //Add action to action sheet
        
        alert.addAction(actionThree)
        
        if let popoverController = alert.popoverPresentationController {
//            popoverController.sourceView = self.view
//            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//            popoverController.permittedArrowDirections = []
            popoverController.sourceView = sender as? UIView
            popoverController.sourceRect = (sender as AnyObject).bounds;
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.right
        }
        //Present alert
        self.present(alert, animated: true, completion: nil)

       }
    }
    
    
    @IBAction func AddEvent(_ sender: Any) {
        
        
    }
    
    
    
    
    
    
    

    
   
    
    
    
    
    
    
    
    
}


extension SparkdetailsViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        animateView(up: true, moveValue: 80)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateView(up: false, moveValue:
            80)
    }
    func textViewDidChange(_ textView: UITextView) {
       commentbox.resolveHashTags()
        
    }
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    

    
}

