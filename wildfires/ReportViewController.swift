//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import Firebase

class ReportViewController: UIViewController  {
    
    
    @IBOutlet weak var descriptiontextview: UITextView!
    
    var networkingService = NetworkingService()
    
    var problem : String = ""
    var reportitemid : String = ""
    var userid : String = ""
    var reportitem : String = "General"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     
        descriptiontextview!.layer.borderWidth = 1
        descriptiontextview!.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func reportbtnaction(_ sender: Any) {
        userid = (Auth.auth().currentUser?.uid)!
        problem = descriptiontextview.text
        
        let postDate = NSDate().timeIntervalSince1970 as NSNumber
        let newReport = Report(reportItemId: reportitemid, userId: userid, reportItem: reportitem, problem: problem, postDate: postDate)
        self.networkingService.addreport(report: newReport) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
 
    
    
    
}






