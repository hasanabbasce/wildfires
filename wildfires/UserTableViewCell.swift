//
//  UserTableViewCell.swift
//  wildfires
//
//  Created by Hasan Abbas on 13/08/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var profileimage: CustomizableImageView!
    @IBOutlet weak var fullname: UILabel!
    @IBOutlet weak var chatbtn: UIButton!
    
    
     var user : Useri?
     var delegate: SparkTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(User: Useri){
         self.chatbtn.tintColor = UIColor.init(colorWithHexValue: "#FF6E95")
        self.profileimage?.sd_setImage(with: URL(string: User.profilePictureUrl!))
        self.fullname?.text = User.fullname
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.profiletap))
        
        tapGesture.numberOfTapsRequired = 1
        self.profileimage.addGestureRecognizer(tapGesture)
        
        let tapGesturelable = UITapGestureRecognizer(target: self, action: #selector(self.profiletap))
        
        tapGesturelable.numberOfTapsRequired = 1
        self.fullname.addGestureRecognizer(tapGesturelable)
        
        self.user = User
    }
    
    func profiletap(_ sender:AnyObject){
        self.delegate.didtapprofilebutton(user: (self.user)!)
        
    }
    
    @IBAction func chatclick(_ sender: Any) {
        self.user?.action = "chat"
        self.delegate.didtapprofilebutton(user: (self.user)!)
        self.user?.action = nil
    }
    
    
    
    
    
}
