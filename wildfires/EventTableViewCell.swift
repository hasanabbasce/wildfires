//
//  EventTableViewCell.swift
//  wildfires
//
//  Created by Hasan Abbas on 19/02/2018.
//  Copyright © 2018 WIP. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    

    @IBOutlet weak var eventdescription: UILabel!
    
    @IBOutlet weak var eventtime: UILabel!
    
    @IBOutlet weak var eventlocation: UILabel!
    
    
    var Event : Event?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(Event: Event){
        self.Event = Event

        self.eventdescription.text = Event.description
       
        let date = Date(timeIntervalSince1970: Event.date as! TimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
         self.eventtime.text = strDate
        if(self.Event?.time != nil)
        {
            self.eventtime.text = strDate + " " + (self.Event?.time)!
        }
        self.eventlocation.text = Event.location
       
        
        
    }
    
}
