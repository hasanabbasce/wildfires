//
//  SearchResultsController.swift
//  PlacesLookup
//
//  Created by Malek T. on 9/30/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit
import GooglePlaces

protocol PlaceSearchResultsControllerDelegate{
    func locateWithLongitude(_ lon:Double, andLatitude lat:Double, andTitle title: String,location_details : [ String : String ])
}
class PlaceSearchResultsController: UITableViewController , UISearchControllerDelegate {
    
    var searchResults: [String]!
    var delegate: PlaceSearchResultsControllerDelegate!
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchResults = Array()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview: (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.shadowImage = UIImage()
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.delegate = self
        self.definesPresentationContext = true
        
        // tableView.tableHeaderView = searchController.searchBar
        navigationItem.hidesBackButton = true
        self.navigationItem.titleView = searchController.searchBar
        if #available(iOS 11.0, *) {
            searchController.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.isActive = true
      
        for subView in searchController.searchBar.subviews {
            
            for subViewOne in subView.subviews {
                
                if let textField = subViewOne as? UITextField {
                    subViewOne.backgroundColor = UIColor(white: 1, alpha: 0.2)
                    
                    //use the code below if you want to change the color of placeholder
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = UIColor.lightGray
                }
            }
        }

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in }) { (completed) -> Void in
            
            self.searchController.searchBar.becomeFirstResponder()
        }
    }
    
    
    
    func didPresentSearchController(_ searchController: UISearchController){
        UIView.animate(withDuration: 0.1, animations: { () -> Void in }) { (completed) -> Void in
            
            self.searchController.searchBar.becomeFirstResponder()
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if let navController = self.navigationController {
            let transition: CATransition = CATransition()
            transition.duration = 1
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionFade
            self.navigationController!.view.layer.add(transition, forKey: nil)
            navController.popViewController(animated: false)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchResults.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        
        cell.textLabel?.text = self.searchResults[indexPath.row]
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath){
     if let navController = self.navigationController {
         navController.popViewController(animated: false)
        }
  
        //self.delegate.locateWithLongitude(Double(0), andLatitude: Double(0), andTitle: self.searchResults[indexPath.row],location_details : [  :  ])
        
        if let correctedAddress = self.searchResults[indexPath.row].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)") {
            print(url)
        
       
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            // 3
            do {
                if data != nil{
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    
                    let results = dic["results"] as! NSArray
                    var location_details = [ String : String ]()
                    for result in results {
                        let strObj = result as! NSDictionary
                        let geometry = strObj["geometry"] as! NSDictionary
                        let location = geometry["location"] as! NSDictionary
                        let lat = location["lat"] as! NSNumber
                        let lon = location["lng"] as! NSNumber
                        
                        for address in strObj["address_components"] as! NSArray
                        {
                            
                            let addressme = address as! NSDictionary
                            
                            let addresstypes : NSArray = addressme["types"] as! NSArray
                            
                            
                            
                            if (String(describing: addresstypes[0]) == "country") {
                                location_details["country"] =  addressme["long_name"] as?  String
                            }
                            if (String(describing: addresstypes[0]) == "administrative_area_level_1") {
                                location_details["state"] =  addressme["long_name"] as? String
                            }
                            if (String(describing: addresstypes[0]) == "administrative_area_level_2") {
                                location_details["country"] =  addressme["long_name"] as? String
                            }
                            if (String(describing: addresstypes[0]) == "locality") {
                                location_details["city"] =  addressme["long_name"] as? String
                            }
                            if (String(describing: addresstypes[0]) == "postal_code") {
                                location_details["postal_code"] =  addressme["long_name"] as? String
                            }
                            if (String(describing: addresstypes[0]) == "route") {
                                location_details["route"] =  addressme["long_name"] as? String
                            }
                            
                          
                        }
                        
                         DispatchQueue.main.async {
                        self.delegate.locateWithLongitude(Double(lon), andLatitude: Double(lat), andTitle: self.searchResults[indexPath.row], location_details : location_details)
                        }
                    }
                    
                }
                
            }catch {
                print("Error")
            }
        })
            
        // 5
        task.resume()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String){
        
       
        
    }
    func filterContentForSearchText(_ searchText: String) {
        let placesClient = GMSPlacesClient()
        
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.searchResults.removeAll()
                for result in results {
                    //    print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID))")
                    
                    self.searchResults.append(result.attributedFullText.string)
                    
                }
            }
            self.reloadDataWithArray()
        })
        
    }
    
    
    
    func reloadDataWithArray(){
        self.tableView.reloadData()
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PlaceSearchResultsController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        // filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension PlaceSearchResultsController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        // let searchBar = searchController.searchBar
        // let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

