
//
//  User.swift
//  FirebaseLive
//
//  Created by Frezy Mboumba on 1/16/17.
//  Copyright © 2017 Frezy Mboumba. All rights reserved.
//

import Foundation
import Firebase

struct Notifications {
    

    var identifier : String?
     var extrastring : String?
    var object : Any?
    
    
    
    
    init( identifier: String, object: Any , extrastring : String = ""){
        
     
        self.object = object
        self.identifier = identifier
        self.extrastring = extrastring
    }
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.identifier != nil)
        {
            initdic["identifier"] = self.identifier!
        }
        if(self.extrastring != nil)
        {
            initdic["extrastring"] = self.extrastring!
        }
        if(self.object != nil)
        {
            initdic["object"] = self.object!
        }
        
        return initdic
    }
    
    
    
}

