//
//  MainTabController.swift
//  wildfires
//
//  Created by Hasan Abbas on 24/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit

class MainTabController: UITabBarController , UITabBarControllerDelegate , UISearchBarDelegate {
    
     
    

    override func viewDidLoad() {
        super.viewDidLoad()
       
         self.delegate = self
        
        
    }
        
    
   
    
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index : Int = (tabBarController.viewControllers?.index(of: viewController))!
        if index == 0
        {
            let RevealViewController = viewController as? SWRevealViewController
            let navigationController = RevealViewController?.frontViewController as! UINavigationController
            navigationController.popToRootViewController(animated: true)
            
        }
    }
  
        
    
    
    
}




