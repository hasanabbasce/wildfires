//
//  ViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 02/07/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GooglePlaces
import Firebase
import NVActivityIndicatorView

class CreateSparkViewController : UIViewController, UITextFieldDelegate , UITextViewDelegate ,CatogoriesTableViewControllerDelegate, PlaceSearchResultsControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate , UINavigationControllerDelegate{
    
    
    @IBOutlet weak var sparktitle: SkyFloatingLabelTextField!

    
    @IBOutlet weak var coverimage: CustomizableImageView!
 
    @IBOutlet weak var Location: SkyFloatingLabelTextField!
    
    @IBOutlet weak var catogories: SkyFloatingLabelTextField!
    
    @IBOutlet weak var descriptiontextview: UITextView!
    
    var sparklocation: String = ""
    var country: String = ""
    var state: String = ""
    var city: String = ""
    var postal_code: String = ""
    var route: String = ""
    var log: Double = 0
    var lat: Double = 0
    
    var tags: [String] = []
    
    var PlaceSearchResultsControllervar:PlaceSearchResultsController!
    var CatogoriesTableViewControllervar:CatogoriesTableViewController!
    
    var netService = NetworkingService()
    let activityData = ActivityData.init(size: nil, message: "Loading ..", messageFont: nil, type: .ballClipRotatePulse, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor(red:0.97, green:0.44, blue:0.41, alpha:0.9), textColor: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        
        setTapGestureRecognizerOnView()
        sparktitle.delegate = self
        Location.delegate = self
        catogories.delegate = self
        descriptiontextview.delegate = self
        descriptiontextview!.layer.borderWidth = 1
        descriptiontextview!.layer.borderColor = UIColor.lightGray.cgColor

        
        PlaceSearchResultsControllervar = PlaceSearchResultsController()
        PlaceSearchResultsControllervar.delegate = self
        CatogoriesTableViewControllervar = CatogoriesTableViewController()
        CatogoriesTableViewControllervar.delegate = self
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapcover(_:)))
        coverimage.addGestureRecognizer(tap)
        coverimage.isUserInteractionEnabled=true
        
        
       self.tags = self.descriptiontextview.resolveHashTags()
        rendercurrentlocation()
    }
    
    func rendercurrentlocation()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.location != nil )
        {
            Location.text = appDelegate.location
            self.sparklocation = appDelegate.location ?? ""
            self.country = appDelegate.country ?? ""
            self.state = appDelegate.state ?? ""
            self.city = appDelegate.city ?? ""
            self.postal_code = appDelegate.postal_code ?? ""
            self.route = appDelegate.route ?? ""
            self.log = appDelegate.lon ?? 0
            self.lat = appDelegate.lat ?? 0
            
        }
    }
    
    func tapcover(_ sender:UITapGestureRecognizer) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = coverimage
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(alertController, animated: true, completion: nil)
    }
   
    
  
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String,location_details : [ String : String ]) {
       
        self.Location.text = title
        self.sparklocation = title
        
        if let country = location_details["country"] {
             self.country = country
        }
        if let state = location_details["state"] {
            self.state = state
        }
        if let city = location_details["city"] {
            self.city = city
        }
        if let postal_code = location_details["postal_code"] {
            self.postal_code = postal_code
        }
        if let route = location_details["route"] {
            self.route = route
        }
       
       
        self.log = lon
        self.lat = lat
        
       print(title)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    
     func imageLayerForGradientBackground(myview : UIView) -> UIImage {
        
        var updatedFrame = myview.bounds
        // take into account the status bar
        updatedFrame.size.height += 20
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func selectedcatogoriesprotocol(_ catogoriesarray: [String]) {
        print(catogories)
        catogories.text = catogoriesarray.joined(separator: ",")
    }
    

    @IBAction func CreateSpark(_ sender: Any) {
        
        let sparkloc = Location?.text!.lowercased()
        let sparktile = sparktitle?.text!
        let sparkcat = catogories?.text!
        let sparkdes = descriptiontextview?.text!
        
        
        if (sparkloc?.isEmpty)! || (sparktile?.isEmpty)! || (sparkcat?.isEmpty)! || (sparkdes?.isEmpty)!  {
            print("empty fields")
        }else {
            let sparkId = NSUUID().uuidString
            let postDate = NSDate().timeIntervalSince1970 as NSNumber
            if let imageData = UIImageJPEGRepresentation(coverimage.image!, CGFloat(0.35)){
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Adding Spark ..  ")
                self.netService.uploadsparkImageToFirebase(sparkId: sparkId, imageData: imageData, completion: { (url) in
                    
                
                    
                    var spark = Spark(sparkId: sparkId, userId: Auth.auth().currentUser!.uid, title: sparktile!, cover: String(describing:url), description: sparkdes!, catogories: sparkcat!, location: self.sparklocation, postDate: postDate, country: self.country, state: self.state, city: self.city, postal_code: self.postal_code, route: self.route ,lat: self.lat,lon: self.log,tags:self.tags)
                        self.netService.addsparkToDB(spark: spark, completed: {
//                        for tag in self.tags
//                        {
//                          
//                            let newtag = Tags(userId: Auth.auth().currentUser!.uid, sparkId: sparkId, hashtag: tag)
//                            
//                            self.netService.addtagToDB(tag: newtag, completed: {
//                                print("all done")
//                                
//                            })
//                            
//                            
//                            
//                        }
                            self.Location?.text = ""
                            self.sparktitle?.text = ""
                            self.catogories?.text = ""
                            self.descriptiontextview?.text = ""
                            self.coverimage.image = UIImage(named:"cover")
                             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            if(appDelegate.myinfo != nil)
                            {
                            spark.profileptictureUrl = appDelegate.myinfo?.profilePictureUrl
                            spark.username = appDelegate.myinfo?.fullname
                           
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let SparkdetailsViewController = storyBoard.instantiateViewController(withIdentifier: "SparkdetailsViewController") as! SparkdetailsViewController
                            SparkdetailsViewController.thisspark = spark
                            self.navigationController?.pushViewController(SparkdetailsViewController, animated: true)
                            }
                            
                        })
                    
                    
                  })
                }
        }
        
        self.view.endEditing(true)
        
        
    }
    
    
    
}

extension CreateSparkViewController {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.coverimage.image = chosenImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
   
    
    func textViewDidBeginEditing(_ textView: UITextView) {
    
         animateView(up: true, moveValue: 150)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateView(up: false, moveValue:
            150)
    }
    func textViewDidChange(_ textView: UITextView) {
      self.tags = textView.resolveHashTags()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sparktitle?.resignFirstResponder()
        Location?.resignFirstResponder()
        catogories?.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        if(textField != catogories && textField != Location)
        {
        
        animateView(up: true, moveValue: 80)
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == Location)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(PlaceSearchResultsControllervar, animated: false)
            return false
        }
        else if(textField == catogories)
        {
            self.view.endEditing(true)
            
            let navigator = navigationController
            navigator?.pushViewController(CatogoriesTableViewControllervar, animated: false)
            return false
            
        }
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField != catogories && textField != Location)
        {
        animateView(up: false, moveValue:
            80)
        }
    }
    
    // Move the View Up & Down when the Keyboard appears
    func animateView(up: Bool, moveValue: CGFloat){
        
        let movementDuration: TimeInterval = 0.3
        let movement: CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        
    }
    
    @objc private func hideKeyboardOnTap(){
        self.view.endEditing(true)
        
    }
    
    func setTapGestureRecognizerOnView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnTap))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
   
   
}

