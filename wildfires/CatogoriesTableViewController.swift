//
//  CatogoriesTableViewController.swift
//  wildfires
//
//  Created by Hasan Abbas on 05/08/2017.
//  Copyright © 2017 WIP. All rights reserved.
//

import UIKit

protocol CatogoriesTableViewControllerDelegate{
    func selectedcatogoriesprotocol(_ catogoriesarray: [String])
}

class CatogoriesTableViewController: UITableViewController {

    var catogories: [String] = ["Children's Rights","Immigration","World/International","Animal Rights", "Criminal Justice", "Education" ,"Economic Justice","Environment","Health and Safety","Human Rights","Politics","Racial Justice","Women","Other"]
    var selectedcatogories: [String] = []
    var delegate: CatogoriesTableViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(myview:  (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismisss))
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        
    }
    
    func dismisss() {
        
        self.delegate.selectedcatogoriesprotocol(self.selectedcatogories)
       
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.catogories.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        
        cell.textLabel?.text = self.catogories[indexPath.row]
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected  \(catogories[indexPath.row])")
        
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.isSelected {
                if(cell.accessoryType == .checkmark)
                {
                    selectedcatogories = selectedcatogories.filter { $0 != catogories[indexPath.row] }
                    cell.accessoryType = .none
                }
                else
                {
                    selectedcatogories.append(catogories[indexPath.row])
                cell.accessoryType = .checkmark
                }
            }
        }
        
        if let sr = tableView.indexPathsForSelectedRows {
            print("didDeselectRowAtIndexPath selected rows:\(sr)")
        }
    }
    
  
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("deselected  \(catogories[indexPath.row])")
        
        if let cell = tableView.cellForRow(at: indexPath) {
         //   cell.accessoryType = .none
        }
        
        if let sr = tableView.indexPathsForSelectedRows {
            print("didDeselectRowAtIndexPath selected rows:\(sr)")
        }
    }
    
    
}



