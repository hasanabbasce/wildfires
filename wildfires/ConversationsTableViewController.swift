//
//  ConversationsTableViewController.swift
//  WhatsAppClone
//
//  Created by Frezy Stone Mboumba on 7/13/16.
//  Copyright © 2016 Frezy Stone Mboumba. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import SCLAlertView


class ConversationsTableViewController: UITableViewController {
    
    var chatFunctions = ChatFunctions()
     var netService = NetworkingService()
    
    var dataBaseRef: DatabaseReference! {
        
        return Database.database().reference()
    }
    
    var storageRef: Storage! {
        return Storage.storage()
    }
    
    var chatsArray = [ChatRoom]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let button: UIButton = UIButton(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "search-active"), for: .normal)
        //add function for button
        button.addTarget(self, action: #selector(ConversationsTableViewController.searchButtonPressed), for: .touchUpInside)
        //set frame
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        //CGRectMake(0, 0, 53, 31)
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
        fetchChats()
        NotificationCenter.default.addObserver(self, selector: #selector(ConversationsTableViewController.fetchChats), name: NSNotification.Name(rawValue: "updateDiscussions"), object: nil)
    }
    
    func searchButtonPressed() {
        
        tabBarController?.selectedIndex = 1
        
        let navController = tabBarController?.viewControllers?[1] as? UINavigationController
        let search = navController?.viewControllers[0] as? UserSearchResultsController
        search?.selectedindex = 1
        tabBarController?.selectedViewController = navController
    }
    
    func fetchChats(){
        chatsArray.removeAll(keepingCapacity: false)
        dataBaseRef.child("ChatRooms").queryOrdered(byChild: "userId").queryEqual(toValue: Auth.auth().currentUser!.uid).observe(.childAdded, with: { (snapshot) in
            
            let username = (snapshot.value as! NSDictionary)["username"] as! String
            let other_Username = (snapshot.value as! NSDictionary)["other_Username"] as! String
            let userId = (snapshot.value as! NSDictionary)["userId"] as! String
            let other_UserId = (snapshot.value as! NSDictionary)["other_UserId"] as! String
            let lastMessage = (snapshot.value as! NSDictionary)["lastMessage"] as? String ?? ""
            let userPhotoUrl = (snapshot.value as! NSDictionary)["userPhotoUrl"] as! String
            let other_UserPhotoUrl = (snapshot.value as! NSDictionary)["other_UserPhotoUrl"] as! String
            let members = (snapshot.value as! NSDictionary)["members"] as! [String]
            let ref = snapshot.ref
            let key = snapshot.key
            let chatRoomId = (snapshot.value as! NSDictionary)["chatRoomId"] as! String
            let date = (snapshot.value as! NSDictionary)["date"] as! NSNumber
            
            
            var newChat = ChatRoom(username: username, other_Username: other_Username, userId: userId, other_UserId: other_UserId, members: members, chatRoomId: chatRoomId, lastMessage: lastMessage, userPhotoUrl: userPhotoUrl, other_UserPhotoUrl:other_UserPhotoUrl,date:date)
            newChat.ref = ref
            newChat.key = key
            if(!self.netService.isblockeduid(uid: other_UserId))
            {
               self.chatsArray.insert(newChat, at: 0)
            }
            
            self.tableView.reloadData()
            
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: error.localizedDescription)
            
            
        }
        
        
        dataBaseRef.child("ChatRooms").queryOrdered(byChild: "other_UserId").queryEqual(toValue: Auth.auth().currentUser!.uid).observe(.childAdded, with: { (snapshot) in
            
            let username = (snapshot.value as! NSDictionary)["username"] as! String
            let other_Username = (snapshot.value as! NSDictionary)["other_Username"] as! String
            let userId = (snapshot.value as! NSDictionary)["userId"] as! String
            let other_UserId = (snapshot.value as! NSDictionary)["other_UserId"] as! String
            let lastMessage = (snapshot.value as! NSDictionary)["lastMessage"] as? String ?? ""
            let userPhotoUrl = (snapshot.value as! NSDictionary)["userPhotoUrl"] as! String
            let other_UserPhotoUrl = (snapshot.value as! NSDictionary)["other_UserPhotoUrl"] as! String
            let members = (snapshot.value as! NSDictionary)["members"] as! [String]
            let ref = snapshot.ref
            let key = snapshot.key
            let chatRoomId = (snapshot.value as! NSDictionary)["chatRoomId"] as! String
            let date = (snapshot.value as! NSDictionary)["date"] as! NSNumber
            
            var newChat = ChatRoom(username: username, other_Username: other_Username, userId: userId, other_UserId: other_UserId, members: members, chatRoomId: chatRoomId, lastMessage: lastMessage, userPhotoUrl: userPhotoUrl, other_UserPhotoUrl: other_UserPhotoUrl,date:date)
            newChat.ref = ref
            newChat.key = key
            
            if(!self.netService.isblockeduid(uid: userId))
            {
                self.chatsArray.insert(newChat, at: 0)
            }
            self.tableView.reloadData()
            
            
            
        }) { (error) in
            let alertView = SCLAlertView()
            alertView.showError("OOPS", subTitle: error.localizedDescription)
            
            
        }
        
        
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return chatsArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "conversationsCell", for: indexPath) as! ConversationsTableViewCell
        
        var userPhotoUrlString: String? = ""
        
        if chatsArray[indexPath.row].userId == Auth.auth().currentUser!.uid {
            userPhotoUrlString = chatsArray[indexPath.row].other_UserPhotoUrl
            cell.usernameLabel.text = chatsArray[indexPath.row].other_Username
        }else {
            userPhotoUrlString = chatsArray[indexPath.row].userPhotoUrl
            cell.usernameLabel.text = chatsArray[indexPath.row].username
        }
        

        
        let fromDate = NSDate(timeIntervalSince1970: TimeInterval(chatsArray[indexPath.row].date))
        let toDate = NSDate()
        
        cell.dateLabel.text  = timeAgoSinceDate(fromDate as Date, currentDate: toDate as Date, numericDates: true)
        
        
        
        
        cell.lastMessageLabel.text = chatsArray[indexPath.row].lastMessage
        if let urlString = userPhotoUrlString {
//            storageRef.reference(forURL: urlString).getData(maxSize: 1 * 1024 * 1024, completion: { (imgData, error) in
//                if let error = error {
//                    let alertView = SCLAlertView()
//                    alertView.showError("OOPS", subTitle: error.localizedDescription)
//                }else {
//                    
//                    DispatchQueue.main.async(execute: {
//                        if let data = imgData {
//                            cell.userImageView.image = UIImage(data: data)
//                        }
//                    })
//                    
//                }
//            })
            
            cell.userImageView.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "default"))
        }
        
        
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var inituserdic = [String: Any]()
        inituserdic["fullname"] = Auth.auth().currentUser!.displayName!
        inituserdic["uid"] = Auth.auth().currentUser!.uid
        inituserdic["profilePictureUrl"] = String(describing: Auth.auth().currentUser!.photoURL!)
        let currentUser = Useri.init(initdic: inituserdic)
        var otherUser: Useri!
        if currentUser.uid == chatsArray[indexPath.row].userId{
            var initotherUserdic = [String: Any]()
            initotherUserdic["fullname"] = chatsArray[indexPath.row].other_Username
            initotherUserdic["uid"] = chatsArray[indexPath.row].other_UserId
            initotherUserdic["profilePictureUrl"] =  chatsArray[indexPath.row].other_UserPhotoUrl
            otherUser = Useri.init(initdic: initotherUserdic)
        }else {
            
            var initotherUserdic = [String: Any]()
            initotherUserdic["fullname"] = chatsArray[indexPath.row].username
            initotherUserdic["uid"] = chatsArray[indexPath.row].userId
            initotherUserdic["profilePictureUrl"] =  chatsArray[indexPath.row].userPhotoUrl
            otherUser = Useri.init(initdic: initotherUserdic)
        }
        
        chatFunctions.startChat(currentUser, user2: otherUser)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let chatVC = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.senderId = Auth.auth().currentUser!.uid
        chatVC.senderDisplayName = Auth.auth().currentUser!.displayName!
        chatVC.chatRoomId = chatFunctions.chatRoom_id
        chatVC.user1 = currentUser
        chatVC.user2 = otherUser
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            self.chatsArray[indexPath.row].ref?.removeValue()
            self.chatsArray.remove(at: indexPath.row)
            self.tableView.reloadData()
            
        }
    }
    
    
    
    
    
    
    
    
    
    
}
